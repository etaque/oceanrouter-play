name := "oceanrouter"

version := "1.0-SNAPSHOT"

resolvers ++= Seq(
  "GeoTools" at "http://download.osgeo.org/webdav/geotools/"
)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "edu.ucar" % "grib" % "8.0.29",
  "org.geotools" % "gt-main" % "10.2",
  "org.geotools" % "gt-referencing" % "10.2",
  "commons-io" % "commons-io" % "2.4",
  "net.sf.supercsv" % "super-csv" % "2.1.0",
  "com.typesafe.play.extras" %% "play-geojson" % "1.0.0"
)

play.Project.playScalaSettings

scalacOptions += "-feature"