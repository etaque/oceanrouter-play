package controllers

import play.api.mvc.{Action, Controller}
import models.{BoatRank, Leaderboard}
import play.api.libs.json.Json

object Leaderboards extends Controller with Secured {

  def current(raceId: Long) = Action {
    Leaderboard.latest(raceId) match {
      case Some(lb) => Ok(Json.obj(
        "time" -> lb.time,
        "ranks" -> BoatRank.byLeaderboardId(lb.id).map {
          case (rank, boat) => Json.obj(
            "rank" -> rank.rank,
            "id" -> boat.id,
            "name" -> boat.name,
            "distance" -> rank.distance
          )
        }
      ))
      case None => NotFound
    }
  }
}
