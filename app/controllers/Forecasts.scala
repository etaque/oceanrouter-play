package controllers

import play.api.mvc.{Action, Controller}
import models.{Forecast, ForecastPoint, Position}
import play.api.libs.json.Json
import play.extras.geojson.FeatureCollection

object Forecasts extends Controller {

  def closest(term: Int, lat: Double, lon: Double) = Action {
    Forecast.lastAvailable(term = term) match {
      case Some(forecast) => ForecastPoint.closest(forecast.id, Position(lat, lon)) match {
        case Some(point) => Ok(Json.toJson(point))
        case None => NotFound
      }
      case None => BadRequest
    }
  }

  def inBox(term: Int, lat1: Double, lon1: Double, lat2: Double, lon2: Double) = Action {
    Forecast.lastAvailable(term = term) match {
      case Some(forecast) => {
        val features = ForecastPoint.inBox(forecast.id, Position(lat1, lon1), Position(lat2, lon2)).map(_.toGeoJson)
        Ok(Json.toJson(Json.obj(
          "type" -> "FeatureCollection",
          "features" -> features,
          "properties" -> Json.obj(
            "genTime" -> forecast.genTime
          )
        )))
      }
      case None => BadRequest
    }
  }

}
