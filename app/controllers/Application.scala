package controllers

import play.api.mvc._
import models._
import play.api.Routes

object Application extends Controller with Secured {

  def index = Authenticated { implicit request =>
    val races = Race.all
    Ok(views.html.index(races))
  }

}