package controllers

import scala.concurrent.duration._
import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json
import play.api.libs.concurrent.Akka
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import akka.util.Timeout
import akka.actor.Props
import akka.pattern.{ ask, pipe }
import core.sim._
import models._

object Simulations extends Controller with Secured {

  var simActor = Akka.system.actorOf(Props[RoutingActor])

  def run(lat1: Double, lon1: Double, lat2: Double, lon2: Double) = Action.async {
    implicit val timeout = Timeout(60.seconds)
    val t0 = System.nanoTime()
    simActor.ask(RoutingRequest(Position(lat1, lon1), Position(lat2, lon2))).map {
      case result: RoutingResult => {
        val duration = ((System.nanoTime() - t0) / 1000000).toInt
        play.Logger.debug(s"simulation done in $duration ms")
        Ok(Json.toJson(result.bestRoute))
      }
    }
  }
}
