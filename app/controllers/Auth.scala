package controllers

import play.api.mvc.{Action, Controller}
import models._
import play.api.data.Form
import play.api.data.Forms._

object Auth extends Controller with Secured {

  val loginForm = Form(tuple(
    "email" -> nonEmptyText,
    "password" -> nonEmptyText
  ))

  def login = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def loginPost = Action { implicit request =>
    val form = loginForm.bindFromRequest()
    form.fold(
    formWithErrors => BadRequest(views.html.login(formWithErrors)),
    {
      case (email, password) => User.findByEmail(email).filter(_.testPassword(password)) match {
        case Some(user) => Redirect(routes.Application.index).withSession("email" -> email)
        case None => BadRequest(views.html.login(form))
      }
    }
    )
  }

  def logout = Action {
    Redirect(routes.Auth.login).withNewSession
  }

}
