package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json
import models.PolarCell

object Polars extends Controller with Secured {

  def forWind(raceId: Long, windSpeed: Double) = Action {
    Ok(Json.toJson(PolarCell.speedByWindAngle("imoca60", windSpeed)))
  }

}
