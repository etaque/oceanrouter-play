package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.{JsValue, Json}
import models._
import utils.Config

object Races extends Controller with Secured {

  def console(id: Long) = Authenticated { implicit request =>
    (for {
      race <- Race.find(id)
      boat <- Boat.byUserId(request.user.id).find(_.raceId == race.id)
      status <- BoatStatus.forBoat(boat)
    } yield {
      val data: JsValue = Json.obj(
        "race" -> race,
        "user" -> request.user,
        "status" -> status,
        "polar" -> status.forecastPoint.map { fp =>
          PolarCell.speedByWindAngle("imoca60", fp.wind.speed)
        },
        "config" -> Json.obj {
          "finishRadius" -> Config.finishRadius
        }
      )
      Ok(views.html.console(race, status, data))
    }) getOrElse InternalServerError
  }


  def consoleReact(id: Long) = Authenticated { implicit request =>
    (for {
      race <- Race.find(id)
      boat <- Boat.byUserId(request.user.id).find(_.raceId == race.id)
      status <- BoatStatus.forBoat(boat)
    } yield {
      val data: JsValue = Json.obj(
        "race" -> race,
        "user" -> request.user,
        "status" -> status,
        "polar" -> status.forecastPoint.map { fp =>
          PolarCell.speedByWindAngle("imoca60", fp.wind.speed)
        },
        "config" -> Json.obj {
          "finishRadius" -> Config.finishRadius
        }
      )
      Ok(views.html.consoleReact(race, status, data))
    }) getOrElse InternalServerError
  }


  def status(raceId: Long) = Authenticated { implicit request =>
    Race.find(raceId) match {
      case Some(race) => Boat.byUserIdAndRaceId(request.user.id, race.id) match {
        case Some(userBoat) => BoatStatus.forBoat(userBoat) match {
          case Some(status) => {
            Ok(Json.toJson(Json.obj(
              "user" -> status,
              "fleet" -> Boat.asFleet(raceId).filter(_._1.id != userBoat.id).map {
                case (boat, position, move) => BoatStatus(boat, position, move)
              }
            )))
          }
          case None => InternalServerError
        }
        case None => BadRequest
      }
      case None => NotFound
    }
  }

}