package controllers

import play.api.mvc.{Action, Controller}
import models._
import play.api.libs.json.{JsValue, Json}
import org.joda.time.DateTime
import scala.Some

object Boats extends Controller with Secured {

  def fleet(raceId: Long) = Action {
    Ok(Json.toJson(Boat.asFleet(raceId).map {
      case (boat, position, move) => BoatStatus(boat, position, move)
    }))
  }

  def byRace(raceId: Long) = Action {
    Race.find(raceId) match {
      case Some(race) => Ok(Json.toJson(Boat.byRaceId(raceId)))
      case None => NotFound
    }
  }

  def status(id: Long) = Action {
    Boat.find(id: Long) match {
      case Some(boat) => {
        BoatStatus.forBoat(boat) match {
          case Some(status) => Ok(Json.toJson(status))
          case None => InternalServerError
        }
      }
      case None => NotFound
    }
  }


  def route(id: Long) = Action {
    Boat.find(id) match {
      case Some(boat) => Ok(Json.toJson(BoatPosition.byBoatId(id)))
      case None => NotFound
    }
  }

  def turn(id: Long, direction: Double) = Authenticated { implicit request =>
    Boat.find(id) match {
      case Some(boat) => {
        if (request.user.id == boat.userId) {
          val turn = BoatTurn.insert(boat.id, DateTime.now, core.ensureDegrees(direction))
          BoatStatus.forBoat(boat) match {
            case Some(status) => Ok(Json.toJson(status.copy(turn = turn)))
            case None => InternalServerError
          }
        }
        else Forbidden
      }
      case None => NotFound
    }
  }

}
