package controllers

import play.api.mvc._

import models._

trait Secured {
  self: Controller =>

  def Authenticated[A](bodyParser: BodyParser[A])(block: AuthenticatedRequest[A] => Result) = Security.Authenticated(
    (r: RequestHeader) => r.session.get("email"),
    (r: RequestHeader) => doLogin(r))(email => Action(bodyParser) {
    request =>
      User.findByEmail(email).map(u =>
        block(AuthenticatedRequest(u, request))
      ).getOrElse(doLogin(request))
  })

  def doLogin(request: RequestHeader) = Redirect(routes.Auth.login())

  def Authenticated(block: AuthenticatedRequest[AnyContent] => Result) = Authenticated[AnyContent](BodyParsers.parse.anyContent)(block)

  def currentUser(implicit request: RequestHeader): Option[User] = {
    request.session.get("email").flatMap(email => User.findByEmail(email))
  }

}

case class AuthenticatedRequest[A](user: User, request: Request[A]) extends WrappedRequest(request)
