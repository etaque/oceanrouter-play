package utils

import play.api.Play
import scala.concurrent.duration._
import scala.collection.JavaConversions._

object Config {
  val conf = Play.current.configuration

  val loopDuration = conf.getInt("loop").getOrElse(60).seconds
  val forecastsTerms: Seq[Int] = conf.getIntList("forecasts.terms").map(_.map(_.toInt).toSeq).getOrElse(Seq(0,6,12,24,48))

  val simulateMoves: Boolean = conf.getBoolean("simulateMoves").getOrElse(false)

  val finishRadius: Double = conf.getDouble("finishRadius").getOrElse(1852)
}
