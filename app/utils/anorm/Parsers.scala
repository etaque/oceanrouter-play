package utils.anorm

import org.joda.time._
import org.joda.time.format._
import anorm._
import org.postgresql.geometric.{PGbox, PGpoint}
import models.{Box, Position}

object JodaDate {

  val formatter = ISODateTimeFormat.dateTime()

  implicit val rowToDateTime: Column[DateTime] = Column.nonNull {
    (value, _) => value match {
      case ts: java.sql.Timestamp => Right(new DateTime(ts.getTime))
      case d: java.sql.Date => Right(new DateTime(d.getTime))
      case str: java.lang.String => Right(formatter.parseDateTime(str))
      case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass))
    }
  }

  implicit val dateTimeToStatement = new ToStatement[DateTime] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: DateTime) {
      s.setTimestamp(index, new java.sql.Timestamp(aValue.getMillis))
    }
  }

  implicit val dateMidnightToStatement = new ToStatement[DateMidnight] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: DateMidnight): Unit = {
      s.setDate(index, new java.sql.Date(aValue.getMillis()))
    }
  }
}

object PositionParser {
  implicit val rowToPosition: Column[Position] = Column.nonNull {
    (value, _) => value match {
      case p: PGpoint => Right(Position(p.y, p.x))
      case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass))
    }
  }

  implicit val positionToStatement = new ToStatement[Position] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: Position): Unit = {
      s.setObject(index, new PGpoint(aValue.lon, aValue.lat))
    }
  }
  
  implicit val boxToStatement = new ToStatement[Box] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: Box): Unit = {
      s.setObject(index, new PGbox(new PGpoint(aValue._1.lon, aValue._1.lat), new PGpoint(aValue._2.lon, aValue._2.lat)))
    }
  }
}

