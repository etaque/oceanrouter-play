package utils

import play.api._
import scala.concurrent.Await
import scala.concurrent.duration._
import core.grib.GribManager

object GetGrib {

  def main(args: Array[String]) {
    val app = new DefaultApplication(new java.io.File("."), this.getClass.getClassLoader, None, Mode.Dev)
    Play.start(app)

    Await.result(GribManager.getLatest, 1.hour)

    Play.stop()
    sys.exit(0)
  }
}
