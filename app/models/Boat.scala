package models

import anorm.SqlParser._
import anorm._
import anorm.~
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps
import play.api.libs.json.{JsValue, Json, Writes}
import org.joda.time.DateTime

case class Boat(id: Long, userId: Long, raceId: Long, name: String, state: String,
                currentPostionId: Option[Long] = None, currentRank: Option[Long] = None,
                startTime: Option[DateTime] = None, finishTime: Option[DateTime] = None)

object Boat {
  import utils.anorm.JodaDate._

  val table = "boats"

  val single = {
    get[Long](table + ".id") ~
      get[Long]("user_id") ~
      get[Long]("race_id") ~
      get[String]("name") ~
      get[String]("state") ~
      get[Option[Long]]("current_position_id") ~
      get[Option[Long]]("current_rank") ~
      get[Option[DateTime]]("start_time") ~
      get[Option[DateTime]]("finish_time")
  } map {
    case id ~ userId ~ raceId ~ name ~ state ~ positionId ~ rank ~ startTime ~ finishTime =>
      Boat(id, userId, raceId, name, state, positionId, rank, startTime, finishTime)
  }

  object State {
    val ALIVE = "alive"
    val AGROUND = "aground"
    val FINISHED = "finished"
  }

  def insert(userId: Long, raceId: Long, name: String, state: String = State.ALIVE): Boat = {
    DB.withConnection { implicit c =>
      val id = SQL(s"INSERT INTO $table (user_id, race_id, name, state) VALUES ({userId}, {raceId}, {name}, {state})").on(
        'userId -> userId,
        'raceId -> raceId,
        'name -> name,
        'state -> state
      ).executeInsert[Long](scalar[Long].single)
      Boat(id, userId, raceId, name, state)
    }
  }

  def find(id: Long): Option[Boat] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE id={id}").on('id -> id).as(single*).headOption
  }

  def byRaceId(raceId: Long): Seq[Boat] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE race_id={raceId}").on('raceId -> raceId).as(single*)
  }

  def ranked(raceId: Long): Seq[(Boat, Double)] = DB.withConnection { implicit c =>
    SQL(s"""
      | SELECT b.*, distance FROM $table b
      | JOIN ${BoatPosition.table} bp ON b.current_position_id=bp.id
      | WHERE race_id=$raceId
      | ORDER BY b.finish_time ASC, bp.distance ASC
      """.stripMargin).
      on('raceId -> raceId).
      as(single ~ get[Double]("distance") *).map { case boat ~ d => (boat, d) }
  }

  def asFleet(raceId: Long): Seq[(Boat, BoatPosition, BoatTurn)] = DB.withConnection { implicit c =>
    SQL(s"""SELECT DISTINCT ON (b.id) b.*, bp.*, bt.* FROM $table b
      JOIN ${BoatPosition.table} bp ON bp.id=b.current_position_id
      JOIN ${BoatTurn.table} bt ON bt.boat_id=b.id
      ORDER BY b.id, bt.time DESC"""
    ).on('raceId -> raceId).as(
      single ~ BoatPosition.single ~ BoatTurn.single map {
        case boat ~ position ~ turn => (boat, position, turn)
      } *
    )
  }

  def updateRank(id: Long, rank: Long): Unit = DB.withConnection { implicit c =>
    SQL(s"UPDATE $table SET current_rank={rank} WHERE id={id}").
      on('id -> id, 'rank -> rank).executeUpdate()
  }
  def aground(id: Long, time: DateTime = DateTime.now): Unit = DB.withConnection { implicit c =>
    SQL(s"UPDATE $table SET state={state} WHERE id={id}").
      on('id -> id, 'state -> State.AGROUND).executeUpdate()
  }

  def finished(id: Long, time: DateTime = DateTime.now): Unit = DB.withConnection { implicit c =>
    SQL(s"UPDATE $table SET state={state}, finish_time={finishTime} WHERE id={id}").
      on('id -> id, 'state -> State.FINISHED, 'finishTime -> time).executeUpdate()
  }

  def updateCurrentPositionId(id: Long, positionId: Long): Unit = DB.withConnection { implicit c =>
    SQL(s"UPDATE $table SET current_position_id={positionId} WHERE id={id}").on('id -> id, 'positionId -> positionId).executeUpdate()
  }

  def byUserId(userId: Long): Seq[Boat] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE user_id={userId}").on('userId -> userId).as(single*)
  }

  def byUserIdAndRaceId(userId: Long, raceId: Long): Option[Boat] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE user_id={userId} AND race_id={raceId}").on(
      'userId -> userId,
      'raceId -> raceId
    ).as(single*).headOption
  }

  def byStateAndRaceId(state: String, raceId: Long): Seq[Boat] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE state={state} AND race_id={raceId}").on(
      'state -> state,
      'raceId -> raceId
    ).as(single*)
  }

  def countByRaceId(raceId: Long): Long = DB.withConnection { implicit c =>
    SQL(s"SELECT count(*) FROM $table WHERE race_id=$raceId").as(scalar[Long].single)
  }

  implicit val boatWrite = Writes[Boat] { b =>
    Json.obj(
      "id" -> b.id,
      "name" -> b.name,
      "state" -> b.state,
      "rank" -> b.currentRank,
      "startTime" -> b.startTime,
      "finishTime" -> b.finishTime
    )
  }
}

