package models

import org.joda.time.DateTime
import anorm.SqlParser._
import anorm._
import anorm.~
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps
import play.api.libs.json.{Json, Writes}

case class Race(id: Long, name: String, startTime: DateTime, startPosition: Position, startDirection: Double, finishPosition: Position)

object Race {
  import utils.anorm.PositionParser._
  import utils.anorm.JodaDate._

  val table = "races"

  val single = {
    get[Long]("id") ~
      get[String]("name") ~
      get[DateTime]("start_time") ~
      get[Position]("start_position") ~
      get[Double]("start_direction") ~
      get[Position]("finish_position")
  } map {
    case id ~ name ~ startTime ~ startPosition ~ startDirection ~ finishPosition =>
      Race(id, name, startTime, startPosition, startDirection, finishPosition)
  }

  def insert(name: String, startTime: DateTime, startPosition: Position, startDirection: Double, finishPostion: Position): Race = {
    DB.withConnection { implicit c =>
      val id = SQL(s"""
        INSERT INTO $table (name, start_time, start_position, start_direction, finish_position)
        VALUES ({name}, {startTime}, {startPosition}, {startDirection}, {finishPosition})
      """).on(
        'name -> name,
        'startTime -> startTime,
        'startPosition -> startPosition,
        'startDirection -> startDirection,
        'finishPosition -> finishPostion
      ).executeInsert[Long](scalar[Long].single)
      Race(id, name, startTime, startPosition, startDirection, finishPostion)
    }
  }

  def find(id: Long): Option[Race] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE id={id}").on('id -> id).as(single*).headOption
  }

  def count: Long = DB.withConnection { implicit c =>
    SQL(s"SELECT count(*) FROM $table").as(scalar[Long].single)
  }

  def all: Seq[Race] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table").as(single*)
  }

  def live: Seq[Race] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE start_time <= {now}").on('now -> DateTime.now).as(single*)
  }

  implicit val raceWrite = Writes[Race] { o =>
    Json.obj(
      "id" -> o.id,
      "name" -> o.name,
      "startTime" -> o.startTime,
      "startDirection" -> o.startDirection,
      "startPosition" -> o.startPosition,
      "finishPosition" -> o.finishPosition
    )
  }
}
