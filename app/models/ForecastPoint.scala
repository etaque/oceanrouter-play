package models

import play.api.libs.json.{Json, Writes}
import anorm.SqlParser._
import anorm._
import anorm.~
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps
import play.extras.geojson.{LatLng, Point, Feature}
import java.sql.Connection

case class ForecastPoint(forecastId: Long, position: Position, wind: Wind) {
  def toGeoJson = Feature(Point(LatLng(position.lat, position.lon)), Some(Json.obj("wind" -> wind)))
}

object ForecastPoint {
  import utils.anorm.PositionParser._

  val table = "forecast_points"

  val single = {
    get[Long]("forecast_id") ~
      get[Position]("position") ~
      get[Double]("u") ~
      get[Double]("v")
  } map {
    case forecastId ~ position ~ u ~ v =>
      ForecastPoint(forecastId, position, Wind(u, v))
  }

  def insert(forecastId: Long, position: Position, wind: Wind): ForecastPoint = {
    DB.withConnection { implicit c =>
      val id = SQL(s"INSERT INTO $table (forecast_id, position, u, v) VALUES ({forecastId}, {position}, {u}, {v})").on(
        'forecastId -> forecastId, 'position -> position, 'u -> wind.u, 'v -> wind.v
      ).executeInsert[Long](scalar[Long].single)
      ForecastPoint(forecastId, position, wind)
    }
  }

  def insertWithTx(forecastId: Long, position: Position, wind: Wind)(implicit tx: Connection): ForecastPoint = {
    SQL(s"INSERT INTO $table (forecast_id, position, u, v) VALUES ({forecastId}, {position}, {u}, {v})").on(
      'forecastId -> forecastId, 'position -> position, 'u -> wind.u, 'v -> wind.v
    ).executeInsert[Long](scalar[Long].single)
    ForecastPoint(forecastId, position, wind)
  }

  def closest(forecastId: Long, position: Position): Option[ForecastPoint] = {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM forecast_points
        WHERE forecast_id={forecastId}
        ORDER BY position <-> {position} LIMIT 1
      """).on(
        'forecastId -> forecastId,
        'position -> position
      ).as(single*).headOption
    }
  }

  def inBox(forecastId: Long, p1: Position, p2: Position): Seq[ForecastPoint]= {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM forecast_points
        WHERE forecast_id={forecastId}
        AND {box} @> position
      """).on(
        'forecastId -> forecastId,
        'box -> (p1, p2)
      ).as(single*)
    }
  }

  implicit val forecastPointWrite = Writes[ForecastPoint] { fp =>
    Json.obj(
      "position" -> fp.position,
      "wind" -> fp.wind
    )
  }
}
