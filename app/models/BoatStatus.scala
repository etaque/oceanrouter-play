package models

import play.api.libs.json.{Writes, Json}

case class BoatStatus(boat: Boat, position: BoatPosition, turn: BoatTurn,
                      route: Seq[BoatPosition] = Seq(), forecastPoint: Option[ForecastPoint] = None)

object BoatStatus {

  def forBoat(boat: Boat): Option[BoatStatus] = {
    for {
      position <- BoatPosition.last(boat.id)
      turn <- BoatTurn.last(boat.id)
      forecast <- Forecast.lastAvailable()
      forecastPoint <- ForecastPoint.closest(forecast.id, position.position)
    } yield {
      BoatStatus(boat, position, turn, BoatPosition.byBoatId(boat.id), Some(forecastPoint))
    }
  }

  implicit val boatWrite = Writes[BoatStatus] { bs =>
    Json.obj(
      "boat" -> bs.boat,
      "position" -> bs.position,
      "turn" -> bs.turn,
      "route" -> bs.route,
      "forecastPoint" -> bs.forecastPoint
    )
  }
}
