package models

import org.joda.time.DateTime
import anorm.SqlParser._
import anorm._
import play.api.Play.current
import play.api.db.DB
import scala.language.postfixOps
import play.api.libs.json.{Json, Writes}

case class BoatTurn(id: Long, boatId: Long, time: DateTime, direction: Double, position: Option[Position] = None)

object BoatTurn {
  import utils.anorm.JodaDate._
  import utils.anorm.PositionParser._

  val table = "boat_turns"

  val single = {
    get[Long](table + ".id") ~
      get[Long]("boat_id") ~
      get[DateTime]("time") ~
      get[Double]("direction") ~
      get[Option[Position]]("position")
  } map {
    case id ~ boatId ~ time ~ direction ~ position =>
      BoatTurn(id, boatId, time, direction, position)
  }


  def insert(boatId: Long, time: DateTime, direction: Double): BoatTurn = {
    DB.withConnection { implicit c =>
      val lastPosition = BoatPosition.last(boatId).map(_.position)
      val id = SQL(s"INSERT INTO $table (boat_id, time, direction, position) VALUES ({boatId}, {time}, {direction}, {position})").on(
        'boatId -> boatId,
        'time -> time,
        'direction -> direction,
        'position -> lastPosition
      ).executeInsert[Long](scalar[Long].single)
      BoatTurn(id, boatId, time, direction, lastPosition)
    }
  }

  def byBoatId(boatId: Long): Seq[BoatTurn] = {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM $table
        WHERE boat_id={boatId}
        ORDER BY time DESC
      """).on(
        'boatId -> boatId
      ).as(single*)
    }
  }

  def last(boatId: Long): Option[BoatTurn] = {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM $table
        WHERE boat_id={boatId}
        ORDER BY time DESC LIMIT 1
      """).on(
        'boatId -> boatId
      ).as(single*).headOption
    }
  }

  implicit val boatTurnWrite = Writes[BoatTurn] { o =>
    Json.obj(
      "id" -> o.id,
      "time" -> o.time,
      "direction" -> o.direction
    )
  }
}
