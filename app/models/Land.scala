package models

import play.api.db.DB
import play.api.Play.current

import anorm.SqlParser._
import anorm._

object Land {
  // CREATE INDEX idx_land_polygons_geom ON land_polygons USING gist(geom);

  def isAground(p: Position): Boolean = DB.withConnection { implicit c =>
    SQL(s"""
      SELECT (count(*) > 0) AS aground
      FROM land_polygons
      WHERE st_contains(geom, ST_GeomFromText('POINT(${p.lon} ${p.lat})', 4326))
      """).as(scalar[Boolean].single)
  }

}
