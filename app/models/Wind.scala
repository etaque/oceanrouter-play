package models

import play.api.libs.json.{Json, Writes}

case class Wind(u: Double, v: Double) {
  val degreeToRadian = 57.29578 // 180/pi

  lazy val direction = (u, v) match {
    case (0, _) => if (v < 0) 360 else 180
    case _ =>
      (if (u < 0) 270 else 90) - math.atan(v / u) * degreeToRadian
  }

  lazy val origin = core.ensureDegrees(direction + 180)

  lazy val speed = math.sqrt(math.pow(u, 2) + math.pow(v, 2))

  def angleTo(boatDirection: Double) = {
    val delta = origin - boatDirection
    math.abs(
      if (delta > 180) delta - 360
      else if (delta <= -180) delta + 360
      else delta
    )
  }
}

object Wind {
  implicit val windWrite = Writes[Wind] { w =>
    Json.obj(
      "u" -> w.u,
      "v" -> w.v,
      "speed" -> w.speed,
      "direction" -> w.direction,
      "origin" -> w.origin
    )
  }
}