package models

import anorm.SqlParser._
import anorm._
import anorm.~
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps
import play.api.libs.json.{Json, Writes}
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt

case class User(id: Long, email: String, name: String, password: String, creationTime: DateTime, updateTime: DateTime) {
  def testPassword(plainText: String): Boolean = BCrypt.checkpw(plainText, password)
}

object User {
  import utils.anorm.JodaDate._

  val table = "users"

  val single = {
    get[Long]("id") ~
      get[String]("email") ~
      get[String]("name") ~
      get[String]("password") ~
      get[DateTime]("creation_time") ~
      get[DateTime]("update_time")
  } map {
    case id ~ email ~ name ~ password ~ creationTime ~ updateTime =>
      User(id, email, name, password, creationTime, updateTime)
  }

  def insertPlain(email: String, name: String, password: String): User = {
    val hashed = hashPassword(password)
    DB.withConnection { implicit c =>
      val now = DateTime.now
      val id = SQL( s"""INSERT INTO $table (email, name, password, creation_time, update_time)
        VALUES ({email}, {name}, {password}, {creationTime}, {updateTime})""").on(
        'email -> email,
        'name -> name,
        'password -> hashed,
        'creationTime -> now,
        'updateTime -> now
      ).executeInsert[Long](scalar[Long].single)
      User(id, email, name, hashed, now, now)
    }
  }

  def hashPassword(plain: String): String = BCrypt.hashpw(plain, BCrypt.gensalt())

  def findByEmail(email: String): Option[User] = DB.withConnection {
    implicit c =>
      SQL(
        s"""
            SELECT * FROM $table
            WHERE lower(email) = {email}
          """
      ).on(
        'email -> email.toLowerCase
      ).as(single*).headOption
  }

  implicit val userWrite = Writes[User] { u =>
    Json.obj(
      "id" -> u.id,
      "name" -> u.name,
      "email" -> u.email
    )
  }
}

