package models

import org.geotools.referencing.GeodeticCalculator
import core._
import play.api.libs.json._

case class Position(lat: Double, lon: Double) {
  require(lat >= -90 && lat <= 90, "lat ±90°")
  require(lon >= -180 && lon <= 180, "lon ±180°")
}

object Position {

  def angleAndDistanceTo(from: Position, to: Position): (Double, Double) = {
    val calc = new GeodeticCalculator()
    calc.setStartingGeographicPoint(from.lon, from.lat)
    calc.setDestinationGeographicPoint(to.lon, to.lat)
    (azimuthToDegrees(calc.getAzimuth), calc.getOrthodromicDistance)
  }
  def move(from: Position, heading: Double, distance: Double): Position = {
    val calc = new GeodeticCalculator()
    calc.setStartingGeographicPoint(from.lon, from.lat)
    calc.setDirection(degreesToAzimuth(heading), distance)
    val lonLat = calc.getDestinationPosition.getCoordinate.toSeq
    new Position(lonLat(1), lonLat(0))
  }

  implicit val format: Format[Position] = Json.format[Position]
}

