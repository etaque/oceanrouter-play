package models

import org.joda.time.DateTime
import anorm.SqlParser._
import anorm._
import play.api.db.DB
import anorm.~
import play.api.libs.json.{Writes, Json}
import play.api.Play.current
import scala.language.postfixOps

case class Leaderboard(id: Long, raceId: Long, time: DateTime)
case class BoatRank(leaderboardId: Long, boatId: Long, rank: Long, distance: Double)

object Leaderboard {
  import utils.anorm.JodaDate._

  val table = "leaderboards"

  val single = {
    get[Long]("id") ~
      get[Long]("race_id") ~
      get[DateTime]("time")
  } map {
    case id ~ raceId ~ time =>
      Leaderboard(id, raceId, time)
  }

  def insert(raceId: Long, time: DateTime): Leaderboard = {
    DB.withConnection { implicit c =>
      val id = SQL(s"""
        INSERT INTO $table (race_id, time)
        VALUES ({raceId}, {time})
      """).on(
        'raceId -> raceId,
        'time -> time
      ).executeInsert[Long](scalar[Long].single)
      Leaderboard(id, raceId, time)
    }
  }

  def latest(raceId: Long): Option[Leaderboard] = DB.withConnection { implicit c =>
    SQL(s"SELECT * FROM $table WHERE race_id=$raceId ORDER BY time DESC LIMIT 1").as(single *).headOption
  }
}

object BoatRank {
  val table = "boat_ranks"

  val single = {
    get[Long]("leaderboard_id") ~
      get[Long]("boat_id") ~
      get[Long]("rank") ~
      get[Double]("distance")
  } map {
    case leaderboardId ~ boatId ~ rank ~ distance =>
      BoatRank(leaderboardId, boatId, rank, distance)
  }

  def insertRank(leaderboardId: Long, boatId: Long, rank: Long, distance: Double): BoatRank = {
    DB.withConnection { implicit c =>
      SQL(s"""
        INSERT INTO $table (leaderboard_id, boat_id, rank, distance)
        VALUES ({leaderboardId}, {boatId}, {rank}, {distance})
      """).on(
        'leaderboardId -> leaderboardId,
        'boatId -> boatId,
        'rank -> rank,
        'distance -> distance
      ).executeInsert()
      BoatRank(leaderboardId, boatId, rank, distance)
    }
  }

  def byLeaderboardId(leaderboardId: Long): Seq[(BoatRank, Boat)] = DB.withConnection { implicit c =>
    SQL(s"""
      SELECT r.*, b.* FROM $table r
      LEFT JOIN ${Boat.table} b on b.id = boat_id
      WHERE leaderboard_id=$leaderboardId ORDER BY rank ASC
    """).as(single ~ Boat.single *).map { case br ~ boat => (br, boat) }
  }

  implicit val boatRankWrite = Writes[BoatRank] { o =>
    Json.obj(
      "boatId" -> o.boatId,
      "rank" -> o.rank,
      "distance" -> o.distance
    )
  }
}