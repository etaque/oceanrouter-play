package models

import org.joda.time.DateTime
import play.api.db.DB
import anorm._
import anorm.SqlParser._
import play.api.Play.current
import scala.language.postfixOps
import play.api.libs.json.{Json, Writes}

case class BoatPosition(id: Long, boatId: Long, time: DateTime, position: Position,
                        speed: Double, direction: Double, distance: Double,
                        aground: Boolean = false, finished: Boolean = false)

object BoatPosition {
  import utils.anorm.PositionParser._
  import utils.anorm.JodaDate._

  val table = "boat_positions"

  val single = {
    get[Long](table + ".id") ~
      get[Long]("boat_id") ~
      get[DateTime]("time") ~
      get[Position]("position") ~
      get[Double]("speed") ~
      get[Double]("direction") ~
      get[Double]("distance") ~
      get[Boolean]("aground") ~
      get[Boolean]("finished")

  } map {
    case id ~ boatId ~ time ~ position ~ speed ~ direction ~ distance ~ aground ~ finished =>
      BoatPosition(id, boatId, time, position, speed, direction, distance, aground, finished)
  }

  def insert(boatId: Long, time: DateTime, position: Position, speed: Double, direction: Double,
             distance: Double = 0, aground: Boolean = false, finished: Boolean = false): BoatPosition = {
    DB.withConnection { implicit c =>
      val id = SQL(s"""
        INSERT INTO $table (boat_id, time, position, speed, direction, distance, aground, finished)
        VALUES ({boatId}, {time}, {position}, {speed}, {direction}, {distance}, {aground}, {finished})"""
      ).on(
        'boatId -> boatId,
        'time -> time,
        'position -> position,
        'speed -> speed,
        'direction -> direction,
        'distance -> distance,
        'aground -> aground,
        'finished -> finished
      ).executeInsert[Long](scalar[Long].single)
      BoatPosition(id, boatId, time, position, speed, direction, distance, aground)
    }
  }

  def byBoatId(boatId: Long): Seq[BoatPosition] = {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM $table
        WHERE boat_id={boatId}
        ORDER BY time DESC
      """).on(
        'boatId -> boatId
      ).as(single*)
    }
  }

  def last(boatId: Long): Option[BoatPosition] = {
    DB.withConnection { implicit c =>
      SQL(s"""
        SELECT * FROM $table
        WHERE boat_id={boatId}
        ORDER BY time DESC LIMIT 1
      """).on(
        'boatId -> boatId
      ).as(single*).headOption
    }
  }

  implicit val boatPositionWrite = Writes[BoatPosition] { bp =>
    Json.obj(
      "id" -> bp.id,
      "time" -> bp.time,
//      "position" -> bp.position,
      "lat" -> bp.position.lat,
      "lon" -> bp.position.lon,
      "speed" -> bp.speed,
      "direction" -> bp.direction,
      "distance" -> bp.distance,
      "aground" -> bp.aground,
      "finished" -> bp.finished
    )
  }
}
