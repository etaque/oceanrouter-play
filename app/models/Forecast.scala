package models

import anorm.SqlParser._
import anorm._
import anorm.~
import org.joda.time.DateTime
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps

case class Forecast(id: Long, filename: String, genTime: DateTime, term: Int, state: String, creationTime: DateTime)

object Forecast {
  import utils.anorm.JodaDate._

  object State {
    val AVAILABLE = "available"
    val INCOMPLETE = "incomplete"
    val ERROR = "error"
  }

  val table = "forecasts"

  val single = {
    get[Long]("id") ~
      get[String]("filename") ~
      get[DateTime]("gen_time") ~
      get[Int]("term") ~
      get[String]("state") ~
      get[DateTime]("creation_time")
  } map {
    case id ~ filename ~ genTime ~ term ~ state ~ creationTime =>
      Forecast(id, filename, genTime, term, state, creationTime)
  }

  def insert(filename: String, genTime: DateTime, term: Int, state: String): Forecast = {
    DB.withConnection { implicit c =>
      val now = DateTime.now
      val id = SQL(s"""
        INSERT INTO $table (filename, gen_time, term, state, creation_time)
        VALUES ({filename}, {genTime}, {term}, {state}, {creationTime})"""
      ).on(
        'filename -> filename,
        'genTime -> genTime,
        'term -> term,
        'state -> state,
        'creationTime -> now
      ).executeInsert[Long](scalar[Long].single)
      Forecast(id, filename, genTime, term, state, now)
    }
  }

  def updateState(id: Long, state: String): Unit = {
    DB.withConnection { implicit c =>
      SQL(s"UPDATE $table SET state={state} WHERE id={id}").on(
        'state -> state,
        'id -> id
      ).executeUpdate
    }
  }

  private def sqlSelectDesc(sub: String) = s"""
        SELECT * FROM forecasts
        WHERE gen_time<={time} AND term={term} $sub
        ORDER BY gen_time DESC LIMIT 1"""

  def lastTried(time: DateTime = DateTime.now, term: Int = 0): Option[Forecast] =
    DB.withConnection { implicit c =>
      SQL(sqlSelectDesc(s"AND state!={error}")).on(
        'time -> time,
        'term -> term,
        'error -> State.ERROR
      ).as(single*).headOption
    }

  def lastAvailable(time: DateTime = DateTime.now, term: Int = 0): Option[Forecast] =
    DB.withConnection { implicit c =>
      SQL(sqlSelectDesc(s"AND state={available}")).on(
        'time -> time,
        'term -> term,
        'available -> State.AVAILABLE
      ).as(single*).headOption
    }
}
