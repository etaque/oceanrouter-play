package models

import anorm.SqlParser._
import anorm._
import anorm.~
import play.api.db.DB
import play.api.Play.current
import scala.language.postfixOps
import play.api.libs.json.{Json, Writes}

case class PolarCell(id: Long, ref: String, windSpeed: Double, windAngle: Int, boatSpeed: Double)

object PolarCell {
  val table = "polar_cells"
  val parser = {
    get[Long]("id") ~
    get[String]("ref") ~
    get[Double]("wind_speed") ~
    get[Int]("wind_angle") ~
    get[Double]("boat_speed")
  } map {
    case id ~ ref ~ windSpeed ~ windAngle ~ speed =>
      PolarCell(id, ref, windSpeed, windAngle, speed)
  }

  def insert(ref: String, windSpeed: Double, windAngle: Int, boatSpeed: Double): PolarCell = {
    DB.withConnection { implicit c =>
      val id = SQL(s"""
        INSERT INTO $table (ref, wind_speed, wind_angle, boat_speed)
        VALUES ({ref}, {windSpeed}, {windAngle}, {boatSpeed})
      """).on(
        'ref -> ref,
        'windSpeed -> windSpeed,
        'windAngle -> windAngle,
        'boatSpeed -> boatSpeed
      ).executeInsert[Long](scalar[Long].single)
      PolarCell(id, ref, windSpeed, windAngle, boatSpeed)
    }
  }

  def forWind(ref: String, windSpeed: Double, windAngle: Double): Option[PolarCell] =
    DB.withConnection { implicit c =>
      SQL(
        s"""
        | SELECT * FROM $table
        | WHERE ref={ref} AND wind_angle={windAngle} AND wind_speed<={windSpeed}
        | ORDER BY wind_speed DESC LIMIT 1
      """.stripMargin).on(
        'ref -> ref,
        'windAngle -> windAngle,
        'windSpeed -> windSpeed
      ).as(parser *).headOption
    }

  def count: Long =
    DB.withConnection { implicit c =>
      SQL(s"SELECT count(*) FROM $table").as(scalar[Long].single)
    }

  def baseWindSpeed(ref: String, windSpeed: Double): Double = {
    DB.withConnection { implicit c =>
      SQL(s"""
        | SELECT wind_speed FROM $table
        | WHERE ref={ref} AND wind_speed<={windSpeed}
        | ORDER BY wind_speed DESC LIMIT 1
        """.stripMargin).on(
        'ref -> ref,
        'windSpeed -> windSpeed
      ).as(scalar[Double].single)
    }
  }

  def speedByWindAngle(ref: String, windSpeed: Double): Seq[PolarCell] = {
    val bws = baseWindSpeed(ref, windSpeed)
    DB.withConnection { implicit c =>
      SQL(s"""
        | SELECT * FROM $table
        | WHERE ref={ref} AND wind_speed={windSpeed}
        | ORDER BY wind_angle ASC
        """.stripMargin).on(
        'ref -> ref,
        'windSpeed -> bws
      ).as(parser *)
    }
  }

  implicit val polarCellWrite = Writes[PolarCell] { o =>
    Json.obj(
      "windAngle" -> o.windAngle,
      "boatSpeed" -> o.boatSpeed
    )
  }
}
