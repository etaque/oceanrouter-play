((data) ->
  "use strict"

  window.app = {}  unless window.app?
  app.data = data


  isUserBoat = (boat) ->
    boat.id is data.status.boat.id
  app.isUserBoat = isUserBoat


  api =
    getBoundPoints: (term, p1, p2) ->
      routes.controllers.Forecasts.inBox term, p1.lat, p1.lng, p2.lat, p2.lng

    getFleet: ->
      routes.controllers.Boats.fleet data.race.id

    getBoats: ->
      routes.controllers.Boats.byRace data.race.id

    turnBoat: (direction) ->
      routes.controllers.Boats.turn data.status.boat.id, direction

    getBoatStatus: (boatId = data.status.boat.id) ->
      routes.controllers.Boats.status boatId

    getRaceStatus: ->
      routes.controllers.Boats.raceStatus(data.race.id)

    getLeaderboard: ->
      routes.controllers.Leaderboards.current data.race.id

    getPolar: (windSpeed) ->
      routes.controllers.Polars.forWind(data.race.id, windSpeed)

  app.api = api


  core =
    ensure360: (d) ->
      (d + 360) % 360

    windAngle: (windOrigin, boatDirection) ->
      delta = windOrigin - boatDirection
      delta180 = switch
        when delta > 180
          delta - 360
        when delta <= -180
          delta + 360
        else
          delta
      Math.abs delta180

    round: (n, precision = 0) ->
      m = Math.pow(10, precision)
      Math.round(n * m) / m

    round1: (n) -> @round(n, 1)
    round2: (n) -> @round(n, 2)

    knotToMps: (knot) ->
      knot * 1.852 * 1000 / 3600

    mpsToKnot: (mps) ->
      mps / 1.852 / 1000 * 3600

    meterToMile: (m) ->
      m / 1.852 / 1000

    boatSpeed: (windAngle, polar) ->
      p = _.findLast(polar, (p) -> p.windAngle <= windAngle)
      if p then p.boatSpeed else 0

    boatSpeedPct: (polar, s) ->
      max = _(polar).map("boatSpeed").max().value()
      pct = s * 100 / max
      core.round1(pct)

    # Returns radians
    angleBetweenPoints: (p1, p2) ->
      if p1[0] is p2[0] and p1[1] is p2[1]
        Math.PI / 2
      else
        Math.atan2 p2[1] - p1[1], p2[0] - p1[0]

    distanceBetweenPoints: (p1, p2) ->
      Math.sqrt Math.pow(p2[1] - p1[1], 2) + Math.pow(p2[0] - p1[0], 2)

    toDegrees: (rad) -> rad * (180 / Math.PI)
    toRadians: (deg) -> deg * Math.PI / 180

    nextPosition: (position, direction, speed, duration) ->
      distanceInKm = speed * duration / 1000
      from = new LatLon(position.lat, position.lon)
      to = from.destinationPoint(direction, distanceInKm)
      lat: to.lat()
      lon: to.lon()


    rotateWind: (direction) ->
      (direction + 270) % 360

    rotateStyle: (angle) ->
      "-webkit-transform: rotate(#{angle}deg);-moz-transform: rotate(#{angle}deg);" +
      "-ms-transform: rotate(#{angle}deg);-o-transform: rotate(#{angle}deg);"

    opacityForWind: (wind) ->
      0.9 #Math.min(0.1 + wind / this.knotToMps(50), 1);

    eventTargetData: (event) ->
      $(event.target).data()

  app.core = core


  tpl = (->

    cpl = (id) ->
      Handlebars.compile $(id).html()

    Handlebars.registerHelper "mpsToKnot", (speed) ->
      core.round2 core.mpsToKnot(speed)

    Handlebars.registerHelper "toDMS", (point) ->
      "" + Geo.toDMS(point.lat) + " / " + Geo.toDMS(point.lon)

    Handlebars.registerHelper "round2", (d) ->
      core.round2 d

    Handlebars.registerHelper "round", (d) ->
      Math.round d

    Handlebars.registerHelper "speedInKnot", (d) ->
      core.round1 core.mpsToKnot(d)

    Handlebars.registerHelper "distanceInMiles", (d) ->
      Math.round core.meterToMile(d)

    Handlebars.registerHelper "localTime", (t) ->
      moment(t).format "D MMMM HH:mm"

    Handlebars.registerHelper "ranked", (r) ->
      s = if r is 1
        "1<sup>er</sup>"
      else if r is 2
        "2<sup>nd</sup>"
      else if r is null
        "NC"
      else
        "#{r}<sup>e</sup>"
      new Handlebars.SafeString(s)

    boatDescription: cpl("#tpl-boat-description")
    startDescription: cpl("#tpl-start-description")
    finishDescription: cpl("#tpl-finish-description")
    userStatus: cpl("#tpl-user-status")
    ranking: cpl("#tpl-ranking")
    forecasts: cpl("#tpl-forecasts")

  )()
  app.tpl = tpl


  ui = (->
    FleetControl = L.Control.extend
      options:
        position: "topright"

      onAdd: ->
        container = L.DomUtil.create("div", "leaflet-control horizontal-control fleet-control")

        btnLocate = L.DomUtil.create("a", "btn-icon btn-locate-user", container)
        btnLocate.href = "#"
        btnLocate.innerHTML = "<span>&nbsp;</span>"

        btnRefresh = L.DomUtil.create("a", "btn-icon btn-refresh", container)
        btnRefresh.href = "#"
        btnRefresh.innerHTML = "<span>&nbsp;</span>"

        btnFleet = L.DomUtil.create("span", "map-btn btn-fleet", container)
        btnFleet.innerHTML = '<label><input type="checkbox" class="toggle-fleet"> Concurrents</label>'

        container

    ForecastControl = L.Control.extend
      options:
        position: "topleft"

      onAdd: ->
        container = L.DomUtil.create("div", "leaflet-control horizontal-control forecast-control")
        container.innerHTML = tpl.forecasts()
        container

    ForecastCells = L.GeoJSON.extend
      options:
        interval: 0.5
        style:
          color: "#fff"
          weight: 1
          opacity: 0.2

      initialize: (options) ->
        L.Util.setOptions(@, options)
        @_layers = {}
        @addData(@getCells())

      getCells: ->
        interval = @options.interval

        meridians = _.map _.range(-180, 180, interval), (i) =>
          @getFeature(@getMeridian(i + interval / 2))

        parallels = _.map _.range(-90, 90, interval), (i) =>
          @getFeature(@getParallel(i + interval / 2))

        "type": "FeatureCollection"
        "features": meridians.concat(parallels)

      getMeridian: (lon) ->
        _.map(_.range(-90, 90), (lat) -> [lon, lat])

      getParallel: (lat) ->
        _.map(_.range(-180, 180), (lon) -> [lon, lat])

      getFeature: (coords, prop = {}) ->
        type: "Feature"
        geometry:
          type: "LineString"
          coordinates: coords
        properties: prop


    windFeatureToMarker: (feature, latlng) ->
      windDirection = Math.round(feature.properties.wind.direction)
      windOrigin = Math.round(feature.properties.wind.origin)
      windSpeed = core.round2(feature.properties.wind.speed)

      label = "Origine : " + windOrigin.toString() + "&deg;, force : " + Math.round(core.mpsToKnot(windSpeed)).toString() + " noeuds"
      opacity = core.opacityForWind(windSpeed)
      fontSize = Math.round(windSpeed * ui.map.getZoom() / 4)

      windClass = "_f" + Math.min(Math.ceil(Math.round(windSpeed * 8 / 20)), 8)
      windIcon = L.divIcon(
        className: "wind-marker"
        html: "<div title=\"" + label + "\" style=\"" + core.rotateStyle(core.rotateWind(windDirection)) + "opacity:" + opacity.toString() + ";font-size: " + fontSize + "px;\" class=\"" + windClass + "\">&#8594;</div>"
      )
      L.marker latlng,
        icon: windIcon

    boatIconTag: (boatId, direction) ->
      "<div id=\"iconBoat#{boatId}\" style=\"#{core.rotateStyle(direction)}\"></div>"

    boatMarker: (status, direction, isUser = false) ->
      boatIcon = L.divIcon(
        className: if isUser then "boat-marker user-boat-marker" else "boat-marker"
        iconSize: new L.Point(20, 20)
        html: @boatIconTag(status.boat.id, direction)
      )
      description = tpl.boatDescription(status)

      marker = L.marker([status.position.lat, status.position.lon],
        icon: boatIcon
      ).bindPopup(description)

      marker.boatStatus = status
      marker.boatId = status.boat.id
      marker

    boatRoute: (status, isUser) ->
      points = _.map(status.route, (p) ->
        new L.LatLng(p.lat, p.lon)
      )
      line = L.polyline(points,
        color: "black"
        opacity: if isUser then 0.5 else 0.2
        weight: (if isUser then 2 else 1)
        dashArray: (if isUser then null else "5, 5")
        lineJoin: "round"
      )
      line

    updateForecastPoints: ->
      p1 = @map.getBounds().getNorthWest()
      p2 = @map.getBounds().getSouthEast()
      term = models.term.get()

      @forecastMarkerGroup.clearLayers()

      if ui.map.getZoom() > 5
        _this = this
        $.ajax(api.getBoundPoints(term, p1, p2)).then (windFeatures) ->
          L.geoJson(windFeatures,
            pointToLayer: ui.windFeatureToMarker
          ).addTo _this.forecastMarkerGroup
          app.models.forecastGenTime.set(windFeatures.properties.genTime)
        _this.forecastMarkerGroup.addTo _this.map

    updateForecastGenTime: (t) ->
      ft = moment.utc(t).format "D MMMM - HH[h] [UTC]"
      $(".forecast-term.current").attr("title", ft)

    updateFleet: (fleetStatus) ->
      @fleetMarkerGroup.clearLayers()

      _.forEach fleetStatus, (status) ->
        ui.boatMarker(status, status.turn.direction).addTo(ui.fleetMarkerGroup)
#        ui.boatRoute(status, false).addTo(ui.fleetMarkerGroup)

      if models.showFleet.get()
        @fleetMarkerGroup.addTo(@map)


    updateUserBoat: (status, sim) ->
      @userMarkerGroup.clearLayers()

      L.circleMarker(status.position, radius: 20, color: "#F5A623", weight: 1, fillOpacity: 0.2).addTo(@userMarkerGroup)

      @userBoatMarker = @boatMarker(status, models.simDirection.get(), true)
      @userBoatMarker.addTo @userMarkerGroup
      ui.boatRoute(status, true).addTo(@userMarkerGroup)

      @userMarkerGroup.addTo @map

      $("#boatStatus").html(tpl.userStatus(
        boatName: status.boat.name
        rank: status.boat.rank
        position: status.position
        windSpeed: status.forecastPoint.wind.speed
        windOrigin: status.forecastPoint.wind.origin
        direction: sim.direction
        windAngle: core.windAngle(status.forecastPoint.wind.origin, sim.direction)
        speed: sim.speed
      ))

    showNextPosition: (actual, next) ->
      @simulationRoute.clearLayers()

      line = L.polyline [actual, next],
        color: "#fff"
        weight: 2
#        dashArray: "5, 1"
      line.addTo(@simulationRoute)

      point = L.circleMarker next,
        radius: 5
        color: "#fff"
        fillColor: "#fff"
        weight: 1
      point.addTo(@simulationRoute)

      @simulationRoute.addTo(@map)

    cancelSimulation: ->
      @simulationRoute.clearLayers()

    updateRanking: (ranking) ->
      @$ranking.html tpl.ranking(ranking)


    findFleetMarker: (boatId) ->
      _.find @fleetMarkerGroup.getLayers(), (m) ->
        m.boatId is boatId

    findBoatMarker: (boatId) ->
      if boatId is data.status.boat.id
        @userBoatMarker
      else
        @findFleetMarker boatId

    focusOnBoat: (boatId) ->
      m = @findBoatMarker(boatId)
      if m
        @map.panTo(m.getLatLng()).setZoom 12
        m.openPopup()

    orthodromicLine: ->
      start = new arc.Coord(data.race.startPosition.lon, data.race.startPosition.lat)
      finish = new arc.Coord(data.race.finishPosition.lon, data.race.finishPosition.lat)
      gc = new arc.GreatCircle(start, finish)
      geoArc = gc.Arc(50)
      points = _.map(geoArc.geometries[0].coords, (c) ->
        new L.LatLng(c[1], c[0])
      )
      line = L.polyline(points,
        color: "#a30000"
        weight: 1
        dashArray: "5, 5, 1, 5"
        opacity: 0.8
        label: "Route orthodromique"
      )
      line

    selectTerm: (clickedLink) ->
      $(".forecast-control a").removeClass "current"
      $(clickedLink).addClass "current"

    showFleet: ->
      @map.addLayer(@fleetMarkerGroup) unless @map.hasLayer(@fleetMarkerGroup)

    hideFleet: ->
      @map.removeLayer(@fleetMarkerGroup)

    toggleFleet: (b) -> if b then @showFleet() else @hideFleet()

    startRefresh: ->
      $(".btn-refresh").addClass("btn-loading")

    stopRefresh: ->
      $(".btn-refresh").removeClass("btn-loading")

    boatControl:

      svg: d3.select("#boatControlCircle")

      windAngleArc: (boatDirection, windOrigin) ->
        d3.svg.arc()
          .innerRadius(95)
          .outerRadius(100)
          .startAngle(core.toRadians(boatDirection))
          .endAngle(core.toRadians(windOrigin))

      drag: d3.behavior.drag().on("drag", ->
        now = [d3.event.x, d3.event.y]
        before = [d3.event.x - d3.event.dx, d3.event.y - d3.event.dy]
        center = [140, 140]
        angleNow = core.toDegrees(core.angleBetweenPoints(center, now))
        angleBefore = core.toDegrees(core.angleBetweenPoints(center, before))
        angleDelta = angleNow - angleBefore
        models.simDirection.modify((d) -> core.ensure360(d + angleDelta))
        models.simActive.set(true)
      )

      setState: (active) ->
        ui.$boatTurnSubmit.attr("disabled", ! active).toggleClass("btn-primary", active).toggleClass("btn-default", ! active)
        ui.$boatTurnCancel.attr("disabled", ! active)
        ui.$boatSpeedBar.toggleClass("progress-striped", active).toggleClass("active", active)

      updateBoatSpeedBar: (polar, s) ->
        pct = core.boatSpeedPct(polar, s)
        ui.$boatSpeedBar.find(".progress-bar").css("width", "#{pct}%")

      prepare: ->
        @updateBoatSpeedBar(models.polar.get(), models.simSpeed.get())

        @svg.append("svg:g").append("path")
          .attr("id", "simWindAngleArc")
          .attr("d", @windAngleArc(models.simDirection.get(), models.windOrigin.get()))
          .attr("transform", "translate(140,140)")

        @svg.call @drag

      update: (direction, windOrigin = models.windOrigin.get()) ->
        @svg.select("#boat")
          .attr("transform", "rotate(#{direction},140,140)")
        @svg.select("#simWindAngleArc")
          .attr("d", @windAngleArc(direction, windOrigin))

    watercolorTileLayer: new L.StamenTileLayer("watercolor")
    greyTileLayer: new L.StamenTileLayer("toner-lite")
    osmTileLayer: L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png",
      attribution: "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors"
    )
    mapboxTileLayer: L.tileLayer("http://{s}.tiles.mapbox.com/v3/etaque.gl1dd2gb/{z}/{x}/{y}.png")
    map: L.map("map", { zoomControl: false })

    startMarker: L.marker(data.race.startPosition).bindPopup(tpl.startDescription())
    finishMarker: L.marker(data.race.finishPosition).bindPopup(tpl.finishDescription())
    finishCircle: L.circle(data.race.finishPosition, data.config.finishRadius)

    forecastMarkerGroup: new L.LayerGroup()
    forecastCells: new ForecastCells()
    fleetMarkerGroup: new L.LayerGroup()
    userMarkerGroup: new L.LayerGroup()
    userBoatMarker: null

    fleetControl: new FleetControl()
    forecastControl: new ForecastControl()

    simulationRoute: new L.LayerGroup()

    $map: $("#map")
    $direction: $("#boatMoveDirection")
    $ranking: $("#ranking")
    $boatTurnSubmit: $("#boatTurnSubmit")
    $boatTurnCancel: $("#boatTurnCancel")
    $boatSpeedBar: $("#boatSpeedBar")

    init: ->
      moment.lang "fr"

      @map.addLayer(@mapboxTileLayer)
        .addLayer(@forecastCells)
        .addControl(@fleetControl)
        .addControl(@forecastControl)
        .addControl(L.control.zoom({position: 'topright'}))
        .addControl(L.control.layers(
          {
            Terrain: @mapboxTileLayer
            Aquarelle: @watercolorTileLayer
            Gris: @greyTileLayer
          },
          {},
          { position: "bottomright"}
        ))
        .addLayer(@orthodromicLine())
        .setView(models.position.get(), 10)

      @startMarker.addTo @map
      @finishMarker.addTo @map
      @finishCircle.addTo @map

      @$direction.val core.round(models.direction.get())
      @boatControl.prepare()
  )()

  app.ui = ui


  models = {
    status: Bacon.Model(data.status)
    fleet: Bacon.Model([])

    term: Bacon.Model(0)
    forecastGenTime: Bacon.Model("")
    showFleet: Bacon.Model(false)

    polar: Bacon.Model(data.polar)

    init: ->
      @direction = @status.lens("turn.direction")
      @rank = @status.lens("boat.rank")
      @position = @status.lens("position")
      @speed = @position.lens("speed")

      @simActive = Bacon.Model(false)
      @simDirection = Bacon.Model(@direction.get())
      @simSpeed = Bacon.Model(@speed.get())
      @simNextPosition = Bacon.Model(null)
      @simulation = Bacon.Model.combine
        active: @simActive
        direction: @simDirection
        speed: @simSpeed
        position12: @simNextPosition

      @windSpeed = @status.lens("forecastPoint.wind.speed")
      @windOrigin = @status.lens("forecastPoint.wind.origin")

      @
  }.init()

  app.models = models


  streams =

    toggleFleet: ui.$map.asEventStream("click", ".toggle-fleet")
      .map (e) -> e.target.checked

    mapRedraws: Bacon.mergeAll(_.map(["resize", "moveend", "zoomend"], (n) ->
      Bacon.fromEventTarget ui.map, n
    ))

#    fetchPolar: $.Bacon.ajax(api.getPolar())

    forecastChanges: ui.$map.asEventStream("click", ".forecast-term")
      .doAction((event) -> ui.selectTerm event.target)
      .map((event) -> parseInt $(event.target).data().term)

    manualRefreshes: ui.$map.asEventStream("click", ".btn-refresh")
      .doAction(".preventDefault").map(true)

    autoRefreshes: Bacon.once(true).merge(Bacon.interval(60 * 1000).map(true))

    locateUserBoat: ui.$map.asEventStream("click", ".btn-locate-user")
      .doAction(".preventDefault").map(data.status.boat.id)

    rankings: Bacon.once(true).merge(Bacon.interval(60 * 1000))
      .map(api.getLeaderboard).ajax()

    rankingShowBoat: ui.$ranking.asEventStream("click", "tbody tr")
      .doAction(".preventDefault").map (e) ->
        $(e.target).closest("tr").data().boatId

    decDirection: $("body").asEventStream("click", ".btn-direction-dec")
      .doAction(".preventDefault")
      .map(-1)

    incDirection: $("body").asEventStream("click", ".btn-direction-inc")
      .doAction(".preventDefault")
      .map(1)

    directionInputChanges: ui.$direction.asEventStream("change")
      .map(".target.value")
      .map(parseInt).map(core.ensure360)

    turnSubmits: $("#boatTurn").asEventStream("submit")
      .doAction(".preventDefault")
      .map -> models.simDirection.get()

    cancelSimulations: $("#boatTurnCancel").asEventStream("click")
      .doAction(".preventDefault")

    wire: ->
      @forecastRefreshes = Bacon.mergeAll(Bacon.once(true), @mapRedraws, models.term.changes())
      @refreshes = Bacon.mergeAll(@manualRefreshes, @autoRefreshes)

      @forecastRefreshes.onValue -> ui.updateForecastPoints()

      @refreshes.map(-> api.getRaceStatus()).doAction(-> ui.startRefresh()).ajax()
        .onValue (raceStatus) ->
          ui.stopRefresh()
          models.status.set(raceStatus.user)
          models.fleet.set(raceStatus.fleet)

      Bacon.mergeAll(@locateUserBoat, @rankingShowBoat)
        .onValue (boatId) -> ui.focusOnBoat(boatId)

      @rankings.onValue (ranking) -> ui.updateRanking(ranking)

      @polarSpeeds = models.simDirection.changes().map (d) ->
        windAngle = core.windAngle(models.windOrigin.get(), d)
        core.boatSpeed(windAngle, models.polar.get())

      @newPolars = models.windSpeed.changes().map((ws) -> api.getPolar(ws)).ajax()

      @cancelSimulations.onValue ->
        models.simDirection.set(models.direction.get())
        models.simActive.set(false)
        ui.cancelSimulation()

      models.status.changes().onValue (s) ->
        # TODO check
        ui.updateUserBoat(s, models.simulation.get())

      models.position.changes().onValue (p) ->
        if models.simActive.get()
          ui.showNextPosition(p, models.simNextPosition.get())

      models.direction.changes().onValue (d) ->
        models.simDirection.set(d)

      models.fleet.changes().onValue (f) -> ui.updateFleet(f)

      models.term.addSource @forecastChanges
      models.forecastGenTime.changes().onValue (t) -> ui.updateForecastGenTime(t)

      models.simActive.changes().skipDuplicates().onValue (active) ->
        ui.boatControl.setState(active)

      models.simDirection.addSource @directionInputChanges

      models.simDirection.apply Bacon.mergeAll(@decDirection, @incDirection).map((m) -> (n) -> m + n )

      models.simDirection.changes().onValue (d) ->
        models.simActive.set(true)
        ui.$direction.val(core.round(d))
        ui.boatControl.update(d)

      models.simulation.changes().onValue (simulation) ->
        ui.updateUserBoat(models.status.get(), simulation)

      models.simSpeed.addSource(@polarSpeeds)
      models.simSpeed.onValue (s) ->
        ui.boatControl.updateBoatSpeedBar(models.polar.get(), s)

      models.simNextPosition.addSource(models.simSpeed.changes().map((s) ->
        core.nextPosition(models.position.get(), models.simDirection.get(), s, 3600*6)
      ))

      models.simNextPosition.changes().onValue (p) ->
        ui.showNextPosition(models.position.get(), p)

      models.showFleet.addSource(@toggleFleet)
      models.showFleet.changes().onValue (b) -> ui.toggleFleet(b)

      models.polar.addSource(@newPolars)

      @turnSubmits.map(api.turnBoat).ajax().onValue (s) ->
        models.simActive.set(false)
        ui.updateUserBoat(s, models.simulation.get())

  app.streams = streams

)(window.data)

app.ui.init()
app.streams.wire()
