import core.{Polar, GameLoop}
import models._
import org.joda.time.DateTime
import play.api.{Mode, Play, Application, GlobalSettings}

object Global extends GlobalSettings {
  override def onStart(app: Application) {
    GameLoop.start()

    if (Play.current.mode == Mode.Dev) bootstrap()
  }

  def bootstrap() {

    if (PolarCell.count == 0) {
      new Polar("/resources/polars/imoca60.csv").load("imoca60")
    }

    if (Race.count == 0) {

      val fleetSize = 5

      val users = Seq(
        User.insertPlain("etaque@gmail.com", "Skiffr", "password"),
        User.insertPlain("louloute@gmail.com", "Louloute", "password"),
        User.insertPlain("colette@gmail.com", "Colette", "password"),
        User.insertPlain("christian@gmail.com", "Christian", "password"),
        User.insertPlain("ronan@gmail.com", "Ronan", "password")
      ) ++ 1.to(fleetSize).map(i => User.insertPlain(s"user$i@mail.com", s"User $i", "password"))

      val boatNames = Seq("Whanaungatanga", "Graal", "Titibouchon", "Escargot", "La Justice") ++
        1.to(fleetSize).map(i => s"Bateau $i")

      val startTime = DateTime.now.minusMinutes(5)
      val startPosition = Position(36.87729, -122.12347)
      val finishPosition = Position(38.68646, -123.55993)
      val (direction, distance) = Position.angleAndDistanceTo(startPosition, finishPosition)
      val race = Race.insert("La Baule Offshore", startTime, startPosition, direction, finishPosition)

      val rand = new util.Random()
      val delta = 10

      users.zip(boatNames).foreach {
        case (user, name) => {
          val b = Boat.insert(user.id, race.id, name)
          BoatPosition.insert(b.id, startTime, race.startPosition, distance, race.startDirection)
          BoatTurn.insert(b.id, startTime, direction + delta / 2 - rand.nextInt(delta))
        }
      }

    }
  }
}
