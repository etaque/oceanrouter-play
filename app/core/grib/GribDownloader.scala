package core.grib

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.io.{FileOutputStream, FileWriter, InputStream, File}
import java.net.URL
import org.apache.commons.io.{IOUtils, FileUtils}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}
import play.api.libs.ws.WS
import play.api.Logger

case class GribDownloadError(message: String) extends Exception(message)
object GribDownloadError {
  def apply(time: DateTime, term: Int, url: String) = {
    new GribDownloadError(s"GRRRRIB failure : ${time.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH"))} / ${term}h -> $url")
  }
}

class GribDownloader(val time: DateTime, val term: Int = 0) {

  val genDay = time.toString(DateTimeFormat.forPattern("yyyyMMdd"))
  val genHour = time.toString(DateTimeFormat.forPattern("HH"))
  val forecastHour = "%02d".format(term)

  lazy val urlScheme1 = "http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/" +
    s"gfs.$genDay$genHour/gfs.t${genHour}z.pgrb2f$forecastHour"

  lazy val urlScheme2 = "http://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/" +
    s"gfs.$genDay$genHour/gfs.t${genHour}z.pgrb2f$forecastHour"

  lazy val urlScheme3 = "http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/" +
    s"gfs.$genDay$genHour/gfs.t${genHour}z.pgrbf$forecastHour.grib2"

  lazy val urlScheme4 = "http://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/" +
    s"gfs.$genDay$genHour/gfs.t${genHour}z.pgrbf$forecastHour.grib2"

  val url = term match {
//    case 0 | 3 | 6 => urlScheme2
    case _ => urlScheme2
  }


  val path = s"tmp/${genDay}T$genHour-$forecastHour-" + url.split("/").reverse.head
  val file = new File(path)

  def download: Future[Try[String]] = {
    val idxUrl = url + ".idx"

    WS.url(idxUrl).get.map { response =>

      GribDownloader.uvRanges(response.body)

    }.map {
      case Some(ranges) => WS.url(url).withHeaders("Range" -> s"bytes=$ranges")
      case None => WS.url(url)

    }.flatMap { reqHolder =>

      Logger.debug(s"Downloading forecast $time/$term with ${reqHolder}")
      reqHolder.get()

    }.map { response =>

      val asStream: InputStream = response.ahcResponse.getResponseBodyAsStream
      val writer = new FileOutputStream(file)
      Try(IOUtils.copy(asStream, writer))

    }.map {

      case Success(_) => Success(url)
      case Failure(e) => Failure(GribDownloadError(time, term, url))
    }
  }

}

object GribDownloader {

  val rangeLinePattern = ".*(U|V)GRD:10 m above ground.*"

  def uvRanges(body: String): Option[String] = {
    type Line = (String, Int)

    def isNextOf(l1: Line) = (l2: Line) => l2._2 == l1._2 + 1

    def extractRangePointer(line: Line): Long = line._1.split(":").toList match {
      case _ :: (cursor: String) :: _ => cursor.toLong
      case _ => sys.error(s"Invalid idx line ${line._2}")
    }

    val lines = body.split("\n").toList.zipWithIndex

    val expectedLines = lines.filter(_._1 matches rangeLinePattern)

    expectedLines match {
      case uv1 :: uv2 :: Nil => lines.find(isNextOf(uv2)) match {
        case Some(nextLine) => {
          val from = extractRangePointer(uv1)
          val to = extractRangePointer(nextLine) - 1
          Some(s"$from-$to")
        }
        case None => {
          play.Logger.warn(s"Unable to extract ${rangeLinePattern.toString}")
          None
        }
      }
      case Nil => {
        play.Logger.error(s"No U/V line found...")
        None
      }
      case _ => {
        play.Logger.error(s"Was expecting U/V lines in: $expectedLines")
        None
      }
    }
  }
}

