package core.grib

import ucar.unidata.io.RandomAccessFile
import ucar.grib.grib2.{Grib2Data, Grib2Record, Grib2Input}
import models.{Wind, Position}
import collection.JavaConversions._
import core._

class GribExtractor(path: String) {

  val file = new RandomAccessFile(path, "r")
  file.order(RandomAccessFile.BIG_ENDIAN)

  val grib = new Grib2Input(file)
  grib.scan(false, false)

  val U_WIND = 2
  val V_WIND = 3

  def recordForComponent(code: Int): Option[Grib2Record] = grib.getRecords.toSeq.find(_.getPDS.getPdsVars.getParameterNumber == code)

  val uRecord = recordForComponent(U_WIND).getOrElse(sys.error(s"U component of wind not found in GRIB file '$path'"))
  val vRecord = recordForComponent(V_WIND).getOrElse(sys.error(s"V component of wind not found in GRIB file '$path'"))

  def dataForRecord(record: Grib2Record): Seq[Float] = {
    val id = record.getId
    val gd = new Grib2Data(file)
    gd.getData(record.getGdsOffset, record.getPdsOffset, id.getRefTime).toSeq
  }

  def extract(f: (Position, Wind) => Unit): Long = {
    play.Logger.info(s"Extracting $path to DB...")

    val gds = uRecord.getGDS.getGdsVars

    dataForRecord(uRecord).zip(dataForRecord(vRecord)).zipWithIndex.map {
      case ((u, v), i) => {
        val lat = gds.getLa1 - (i / gds.getNx) * gds.getDy
        val lon = degreesToAzimuth(gds.getLo1 + (i % gds.getNx) * gds.getDx)
        val p = Position(lat, lon)
        f(p, Wind(u, v))
      }
    }.length
  }
}

