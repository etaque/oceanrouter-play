package core.grib

import java.sql.DriverManager

import org.joda.time.{Interval, DateTimeZone, DateTime}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import models._
import utils.Config
import scala.util.Try
import play.api.{Play, Logger}
import play.api.db.DB
import play.api.Play.current
import org.postgresql.copy.PGCopyOutputStream
import org.postgresql.core.BaseConnection
import scala.util.Failure
import scala.util.Success

object GribManager {

  val availabilityDelta = 4

  def asMultipleOf6(h: Int) = h / 6 * 6

  def hoursBetween(t1: DateTime, t2: DateTime) = new Interval(t1, t2).toDuration.getStandardHours

  def ensureLatest(): Unit =
    Forecast.lastTried() match {
      case Some(f) => {
        if(hoursBetween(f.creationTime, DateTime.now(DateTimeZone.UTC)) > 6) getLatest
      }
      case None => {
        getLatest
      }
    }

  def getLatest = {
    val now = DateTime.now(DateTimeZone.UTC)
    val time = new DateTime(now.toDateMidnight).withHourOfDay(asMultipleOf6(now.getHourOfDay - availabilityDelta))

    get(time)
  }

  def get(time: DateTime): Future[Seq[Try[Forecast]]] = {

    Future.sequence(Config.forecastsTerms.map { term =>
      val dl = new GribDownloader(time, term)
      dl.download.map {
        case Success(_) => {
          val forecast = Forecast.insert(dl.path.split("/").reverse.head, time, term, Forecast.State.INCOMPLETE)
          Logger.info(s"Extracting forecast $forecast")

          processFile(forecast, dl.path) match {
            case Success(count) => {
              Forecast.updateState(forecast.id, Forecast.State.AVAILABLE)
              Logger.info(s"Successfully extracted $count points from forecast $forecast")
              Success(forecast.copy(state = Forecast.State.AVAILABLE))
            }
            case Failure(e) => {
              Logger.error(e.getMessage)
              Failure(e)
            }
          }
        }
        case Failure(e) => {
          Logger.error(e.getMessage)
          Failure(e)
        }
      }
    })
  }

  def processFile(forecast: Forecast, path: String): Try[Long] = {
    val extractor = new GribExtractor(path)


    //    DB.withConnection { implicit cc =>
    //      cc.getInternalConnection()
    //    }
    val dbUrl = Play.current.configuration.getString("db.default.url").getOrElse(sys.error("db conf not found"))
    val c = DriverManager.getConnection(dbUrl)

    try {
      val bc = c.asInstanceOf[BaseConnection]
      val copyStream = new PGCopyOutputStream(bc, s"COPY ${ForecastPoint.table} FROM STDIN")
      val count = extractor.extract(withCOPY(forecast, copyStream))
      copyStream.endCopy()

      Success(count)

    } finally {
      c.close()
    }
  }

  def withInsert(forecast: Forecast)(p: Position, w: Wind): Unit = ForecastPoint.insert(forecast.id, p, w)

  def withCOPY(forecast: Forecast, copyStream: PGCopyOutputStream)(position: Position, wind: Wind): Unit = {
    val cols = Seq(forecast.id.toString, s"(${position.lon},${position.lat})", wind.u.toString, wind.v.toString)
    val line = s"${cols.mkString("\t")}\n"
    copyStream.writeToCopy(line.getBytes, 0, line.getBytes.length)
  }
}
