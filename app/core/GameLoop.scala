package core

import play.api.Play.current
import play.api.libs.concurrent.Akka
import scala.concurrent.duration._
import models._
import utils.Config
import org.joda.time.DateTime
import core.grib.GribManager
import org.postgresql.copy.PGCopyOutputStream
import org.postgresql.core.BaseConnection
import play.api.db.DB

object GameLoop {

  def start() {
    play.Logger.info("Starting game loop")

    Akka.system.scheduler.schedule(10.seconds, Config.loopDuration) {
      gameLoop()
    }
    Akka.system.scheduler.schedule(10.seconds, 1.minute) {
      forecastLoop()
    }
    Akka.system.scheduler.schedule(20.seconds, 10.minutes) {
      rankingLoop()
    }
  }

  def gameLoop(): Unit = {
    play.Logger.debug(s"Game loop at ${DateTime.now}")
    Race.live.foreach(BoatHandler.raceLoop)
  }

  def rankingLoop(): Unit = {
    play.Logger.debug(s"Ranking loop at ${DateTime.now}")
    Race.live.foreach(RaceCommittee.rankRace)
  }

  def forecastLoop(): Unit = {
    play.Logger.debug(s"Forecast loop at ${DateTime.now}")
    GribManager.ensureLatest()
  }

}
