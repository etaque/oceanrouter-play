package core

import models._
import scala.annotation.tailrec
import play.api.{Mode, Play, Logger}
import scala.Some
import utils.Config
import org.joda.time.{Period, DateTime}

class BoatHandler(race: Race, boat: Boat) {

//  val polar = Polar.current

  val lastPosition = BoatPosition.last(boat.id).getOrElse(sys.error(s"Boat ${boat.id} without first position!"))
  next(lastPosition)

  def debug(msg: String) = Unit //Logger.debug(s"[Boat=${boat.name}] $msg")

  @tailrec
  final def next(currentPosition: BoatPosition): BoatPosition = {

    val now = DateTime.now

    val nextTime = currentPosition.time.plusMillis(Config.loopDuration.toMillis.toInt)
    if (nextTime.isAfterNow) {
      currentPosition

    } else {
      debug(s"current position: ${currentPosition.position} heading to ${currentPosition.direction}")

      Forecast.lastAvailable(currentPosition.time) match {
        case None => {
          Logger.error(s"Forecast not found at ${currentPosition.time}")
          currentPosition
        }

        case Some(forecast) => {

          ForecastPoint.closest(forecast.id, currentPosition.position) match {
            case None => {
              Logger.error(s"Forecast point not found for ${forecast.id} at ${currentPosition.position}")
              currentPosition
            }

            case Some(ForecastPoint(_, _, wind)) => {
              debug(s"wind: from ${wind.direction.toInt} degrees at ${round2(wind.speed)} m/s")

              if (Config.simulateMoves && Play.current.mode == Mode.Dev) {
                val rand = new util.Random()
                if (rand.nextInt(5) == 0 && boat.name != "Whanaungatanga") {
                  BoatTurn.last(boat.id) match {
                    case Some(m) => BoatTurn.insert(boat.id, new DateTime().minusSeconds(10), m.direction + 5 - rand.nextInt(10))
                    case None =>
                  }
                }
              }

              val direction = BoatTurn.last(boat.id).map(_.direction).getOrElse(race.startDirection)
              debug(s"direction: ${direction.toInt} degrees")

              val angleToWind = wind.angleTo(direction)
              debug(s"angle to wind: ${angleToWind.toInt} degrees")

              val boatSpeed = PolarCell.forWind("imoca60", wind.speed, angleToWind.round.toInt).map(_.boatSpeed).getOrElse(0.0)
              debug(s"boat speed: ${round2(boatSpeed)} m/s")

              val distanceToNext = boatSpeed * Config.loopDuration.toSeconds
              debug(s"distance traveled: ${distanceToNext.toInt} m")

              val nextPosition = Position.move(currentPosition.position, direction, distanceToNext)
              debug(s"next position: ${nextPosition}")

              val distanceToFinish = Position.angleAndDistanceTo(nextPosition, race.finishPosition)._2
              val finished = distanceToFinish < Config.finishRadius

              val aground = Land.isAground(nextPosition)

              if (finished) {
                Logger.info(s"Boat #${boat.id} FINISHED!")
                Boat.finished(boat.id, nextTime)
              }
              else if (aground) {
                Logger.info(s"Boat #${boat.id} AGROUND!")
                Boat.aground(boat.id, nextTime)
              }

              val nextBoatPosition = BoatPosition.insert(currentPosition.boatId, nextTime, nextPosition,
                boatSpeed, currentPosition.direction, distanceToFinish, aground, finished)
              debug(s"INSERT Ok")

              Boat.updateCurrentPositionId(boat.id, nextBoatPosition.id)

              Logger.debug(s"Boat #${boat.id} done in ${new Period(now, DateTime.now).getMillis} ms")

              if (!aground && !finished) next(nextBoatPosition)
              else currentPosition
            }
          }
        }
      }
    }
  }

}

object BoatHandler {
  def raceLoop(race: Race): Unit = {
    Boat.byStateAndRaceId(Boat.State.ALIVE, race.id).par.foreach { boat =>
      new BoatHandler(race, boat)
    }
  }
}

