package core.sim

import models.Position
import org.joda.time.DateTime
import play.api.libs.json.Json

case class Log(position: Position, time: DateTime,
               speed: Double, heading: Double,
               windSpeed: Double, windHeading: Double)

object Log {
  implicit val format = Json.format[Log]
}
