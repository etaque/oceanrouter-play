package core.sim

import play.api.libs.json._
import models.{PolarCell, ForecastPoint, Forecast, Position}
import org.joda.time.DateTime


case class Route(journey: Journey, path: List[Log], position: Position,
                 time: DateTime, finished: Boolean = false) {

  lazy val (headingFromOrigin, distanceFromOrigin) = Position.angleAndDistanceTo(journey.origin, position)
  lazy val (headingToDest, distanceToDest) = Position.angleAndDistanceTo(position, journey.dest)
  lazy val (headingFromDest, _) = Position.angleAndDistanceTo(journey.dest, position)

  lazy val convergence = distanceToDest < distanceFromOrigin

  lazy val wind = Forecast.lastAvailable(time).flatMap(forecast => ForecastPoint.closest(forecast.id, position)).map(_.wind)
    .getOrElse(sys.error(s"Wind not found at $time for position $position"))

  def speedOn(heading: Double) = {
    val windAngle = wind.angleTo(heading).round.toInt
    PolarCell.forWind("imoca60", wind.speed, windAngle).map(_.boatSpeed)
      .getOrElse(sys.error(s"Polar speed not found for wind ${wind.speed} and heading $windAngle"))

  }

  def next(heading: Double): Route = {
    val nextTime = time.plusSeconds(journey.stepDuration)
    val speed = speedOn(heading)
    val distance = speed * journey.stepDuration

    val log = new Log(position, time, speed, heading, wind.speed, wind.direction)

    if (heading == headingToDest && distance >= distanceToDest) {
      // success
      val destTime = time.plusSeconds((distanceToDest / speed).toInt)
      new Route(journey, path :+ log, Position.move(position, heading, distanceToDest), destTime, true)
    }
    else new Route(journey, path :+ log, Position.move(position, heading, distance), nextTime)
  }
}

object Route {
  implicit val format: Format[Route] = Json.format[Route]
}

