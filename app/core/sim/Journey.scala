package core.sim

import models.Position
import org.joda.time.DateTime
import play.api.libs.json.{Json, Format}

case class Journey(origin: Position, dest: Position, stepDuration: Int) {

  def this(origin: Position, dest: Position) = this(origin, dest, 60*60*1)

  val (heading, distance) = Position.angleAndDistanceTo(origin, dest)
  val (backHeading, _) = Position.angleAndDistanceTo(dest, origin)

  def start(at: DateTime): Route = new Route(this, List[Log](), origin, at)
}

object Journey {
  implicit val format: Format[Journey] = Json.format[Journey]
}
