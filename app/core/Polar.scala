package core

import org.supercsv.io.CsvListReader
import org.supercsv.prefs.CsvPreference
import java.io.InputStreamReader
import scala.collection.JavaConversions._
import annotation.tailrec
import scala.math._
import models.PolarCell

class Polar(csvPath: String) {
  type Matrix = Seq[Seq[String]]

  val source = new InputStreamReader(getClass.getResourceAsStream(csvPath))
  val csv = new CsvListReader(source, CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE)
  val matrix = readLines(csv, Seq[Seq[String]]())

  val speedParser = (s: String) => round2(knotToMps(s.toDouble))

  lazy val windSpeeds: Seq[Double] = matrix.head.tail.map(speedParser).sorted
  lazy val windAngles: Seq[Int] = matrix.tail.map(_.head).map(_.toInt).sorted
  val speeds: Seq[Seq[Double]] = matrix.tail.map(_.tail.map(speedParser))

  csv.close()

  def load(withRef: String): Unit = {
    windSpeeds.foreach { windSpeed =>
      0.to(180).foreach { windAngle =>
        val boatSpeed = extrapolateSpeedFor(windSpeed, windAngle)
        PolarCell.insert(withRef, windSpeed, windAngle, boatSpeed)
      }
    }
  }

  def extrapolateSpeedFor(windSpeed: Double, windAngle: Int): Double = {
    val speedsForAngle: Seq[(Int,Double)] = byWindSpeed(windSpeed)

    val (angleUnder, speedUnder) = speedsForAngle.reverse.find(_._1 >= windAngle).getOrElse(
      sys.error(s"Polar error: windAngle=$windAngle windSpeed=$windSpeed"))

    val (angleOver, speedOver) = speedsForAngle.find(_._1 >= windAngle).getOrElse(
      sys.error(s"Polar error: windAngle=$windAngle windSpeed=$windSpeed"))

    if (angleUnder == angleOver)
      speedUnder

    else // interpolation linéaire
      speedUnder + (windAngle - angleUnder) * (speedOver - speedUnder) / (angleOver - angleUnder)
  }

  @tailrec
  private def readLines(csv: CsvListReader, acc: Matrix): Matrix = {
    val line = Option(csv.read())
    if (line.isDefined) readLines(csv, acc :+ line.get.toSeq)
    else acc
  }

  def windAngleIndex(angle: Double): Int = windAngles.lastIndexWhere(_ <= angle)

  def byWindSpeed(windSpeed: Double): Seq[(Int, Double)] = {
    val wsIndex = windSpeeds.lastIndexWhere(_ <= windSpeed)
    windAngles zip speeds.map(_(wsIndex))
  }

  def vmgValue(pair: (Int, Double)): Double = pair match {
    case (angle, speed) => abs(cos(toRadians(angle)) * speed)
  }

  def bestVmg(windSpeed: Double, sideFilter: ((Int, Double)) => Boolean): Int =
    byWindSpeed(windSpeed).filter(sideFilter).maxBy(vmgValue)._1

  def lowVmg(windSpeed: Double): Int = bestVmg(windSpeed, { _._1 > 90 })
  def highVmg(windSpeed: Double): Int = bestVmg(windSpeed, { _._1 < 90 })
}

