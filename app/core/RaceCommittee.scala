package core

import org.joda.time.DateTime
import models.{Race, Leaderboard, BoatRank, Boat}

object RaceCommittee {

  def rankRace(race: Race): Unit = {
    val leaderboard = Leaderboard.insert(race.id, DateTime.now)

    Boat.ranked(race.id).zipWithIndex.foreach {
      case ((boat, distance), i) => {
        val rank = i + 1
        BoatRank.insertRank(leaderboard.id, boat.id, rank, distance)
        Boat.updateRank(boat.id, rank)
      }
    }
  }

}
