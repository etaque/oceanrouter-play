# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET         /                                                          controllers.Application.index

GET         /login                                                     controllers.Auth.login
POST        /login                                                     controllers.Auth.loginPost
GET         /logout                                                    controllers.Auth.logout

GET         /console/:id                                               controllers.Races.console(id: Long)
GET         /console-react/:id                                               controllers.Races.consoleReact(id: Long)

GET         /api/forecasts/:term/closest/:lat,:lon                     controllers.Forecasts.closest(term: Int, lat: Double, lon: Double)
GET         /api/forecasts/:term/in-box/:lat1,:lon1/:lat2,:lon2        controllers.Forecasts.inBox(term: Int, lat1: Double, lon1: Double, lat2: Double, lon2: Double)

GET         /api/races/:raceId/polar/:windSpeed                        controllers.Polars.forWind(raceId: Long, windSpeed: Double)

GET         /api/races/:raceId/status                                  controllers.Races.status(raceId: Long)
GET         /api/races/:raceId/fleet                                   controllers.Boats.fleet(raceId: Long)
GET         /api/races/:raceId/boats                                   controllers.Boats.byRace(raceId: Long)
GET         /api/races/:raceId/leaderboard                             controllers.Leaderboards.current(raceId: Long)

GET         /api/boats/:id/status                                      controllers.Boats.status(id: Long)
GET         /api/boats/:id/route                                       controllers.Boats.route(id: Long)
PUT         /api/boats/:id/turn/:direction                             controllers.Boats.turn(id: Long, direction: Float)

GET         /api/simulation/:lat1,:lon1/:lat2,:lon2                    controllers.Simulations.run(lat1: Double, lon1: Double, lat2: Double, lon2: Double)

# Map static resources from the /public folder to the /assets URL path
GET         /assets/*file                                              controllers.Assets.at(path="/public", file)
