# --- !Ups

CREATE TABLE users (
  id            SERIAL8     NOT NULL,
  email         TEXT        NOT NULL UNIQUE,
  name          TEXT        NOT NULL UNIQUE,
  password      TEXT        NOT NULL,
  creation_time TIMESTAMPTZ NOT NULL,
  update_time   TIMESTAMPTZ NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE races (
  id              SERIAL8     NOT NULL,
  name            TEXT        NOT NULL,
  start_time      TIMESTAMPTZ NOT NULL,
  start_position  POINT       NOT NULL,
  start_direction FLOAT8      NOT NULL,
  finish_position POINT       NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE boats (
  id                  SERIAL8 NOT NULL,
  race_id             INTEGER NOT NULL REFERENCES races (id) ON DELETE CASCADE,
  color               TEXT,
  name                TEXT    NOT NULL,
  state               TEXT    NOT NULL DEFAULT 'alive',
  user_id             INTEGER NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  current_position_id INTEGER,
  current_rank        INT8,
  start_time          TIMESTAMPTZ,
  finish_time         TIMESTAMPTZ,

  PRIMARY KEY (id),
  CONSTRAINT race_id_user_id_unique UNIQUE (race_id, user_id)
);

CREATE TABLE boat_positions (
  id        SERIAL8     NOT NULL,
  boat_id   INTEGER     NOT NULL REFERENCES boats (id) ON DELETE CASCADE,
  time      TIMESTAMPTZ NOT NULL,
  position  POINT       NOT NULL,
  speed     FLOAT8      NOT NULL,
  direction FLOAT8      NOT NULL,
  distance  FLOAT8      NOT NULL DEFAULT 0,
  aground   BOOLEAN     NOT NULL DEFAULT FALSE,
  finished  BOOLEAN     NOT NULL DEFAULT FALSE,

  PRIMARY KEY (id)
);

CREATE TABLE boat_turns (
  id        SERIAL8     NOT NULL,
  boat_id   INTEGER     NOT NULL REFERENCES boats (id) ON DELETE CASCADE,
  time      TIMESTAMPTZ NOT NULL,
  direction FLOAT8      NOT NULL,
  position  POINT,

  PRIMARY KEY (id)
);

CREATE TABLE leaderboards (
  id      SERIAL8     NOT NULL,
  race_id SERIAL8     NOT NULL REFERENCES races (id) ON DELETE CASCADE,
  time    TIMESTAMPTZ NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE boat_ranks (
  leaderboard_id SERIAL8 NOT NULL REFERENCES leaderboards (id) ON DELETE CASCADE,
  boat_id        SERIAL8 NOT NULL REFERENCES boats (id) ON DELETE CASCADE,
  distance       FLOAT8  NOT NULL DEFAULT 0,
  rank           INT8    NOT NULL,

  PRIMARY KEY (leaderboard_id, boat_id)
);

CREATE TABLE polar_cells (
  id         SERIAL8 NOT NULL,
  ref        VARCHAR NOT NULL,
  wind_speed FLOAT8  NOT NULL,
  wind_angle INT4    NOT NULL,
  boat_speed FLOAT8  NOT NULL,

  PRIMARY KEY (id),
  CONSTRAINT polar_unique UNIQUE (ref, wind_speed, wind_angle)
)

# --- !Downs

DROP TABLE IF EXISTS polar_cells;
DROP TABLE IF EXISTS boat_ranks;
DROP TABLE IF EXISTS leaderboards;
DROP TABLE IF EXISTS boat_turns;
DROP TABLE IF EXISTS boat_positions;
DROP TABLE IF EXISTS boats;
DROP TABLE IF EXISTS races;
DROP TABLE IF EXISTS users;