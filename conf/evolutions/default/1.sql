# --- !Ups

CREATE TABLE forecasts (
  id            SERIAL8     NOT NULL,
  filename      TEXT        NOT NULL,
  gen_time      TIMESTAMPTZ NOT NULL,
  term          INT         NOT NULL,
  state         VARCHAR     NOT NULL,

  creation_time TIMESTAMPTZ NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE forecast_points (
  forecast_id INTEGER NOT NULL REFERENCES forecasts (id) ON DELETE CASCADE,
  position    POINT NOT NULL,
  u           FLOAT8  NOT NULL,
  v           FLOAT8  NOT NULL
);

CREATE INDEX index_forecast_points_position ON forecast_points USING gist(position);

# --- !Downs

DROP TABLE forecast_points;
DROP TABLE forecasts;