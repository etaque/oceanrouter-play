var Bacon = require('bacon.model');
var data = require('./appData').data;

var models = {

  status: Bacon.Model(data.status),
  fleet: Bacon.Model([]),
  term: Bacon.Model(0),
  forecastGenTime: Bacon.Model(""),
  polar: Bacon.Model(data.polar),
  rankings: Bacon.Model([]),
  zoom: Bacon.Model(10),

  focusedBoatStatus: Bacon.Model(null),

  contextPosition: Bacon.Model(null),

  routing: {
    inProgress: Bacon.Model(false),
    bestRoute: Bacon.Model(null),
  },

  init: function() {
    this.direction = this.status.lens("turn.direction");
    this.rank = this.status.lens("boat.rank");
    this.position = this.status.lens("position");
    this.speed = this.position.lens("speed");

    this.simActive = Bacon.Model(false);
    this.simDirection = Bacon.Model(this.direction.get());
    this.simSpeed = Bacon.Model(this.speed.get());
    this.simNextPosition = Bacon.Model(null);

    this.simulation = Bacon.Model.combine({
      active: this.simActive,
      direction: this.simDirection,
      speed: this.simSpeed,
      position12: this.simNextPosition
    });

    this.windSpeed = this.status.lens("forecastPoint.wind.speed");
    this.windOrigin = this.status.lens("forecastPoint.wind.origin");

    this.focusOnUser = function() {
      this.focusedBoatStatus.set(this.status.get());
    }.bind(this);

    return this;
  }

};


module.exports = models;