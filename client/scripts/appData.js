
var routes = require('./routes');

var data = window.appData;

function api() {
  return {
    getBoundPoints: (term, p1, p2) => routes.Forecasts.inBox(term, p1.lat, p1.lng, p2.lat, p2.lng),

    getFleet: () => routes.Boats.fleet(data.race.id),

    getBoats: () => routes.Boats.byRace(data.race.id),

    turnBoat: (direction) => routes.Boats.turn(data.status.boat.id, direction),

    getBoatStatus: (boatId) => routes.Boats.status(boatId == null ? data.status.boat.id : boatId),

    getRaceStatus: () => routes.Races.status(data.race.id),

    getLeaderboard: () => routes.Leaderboards.current(data.race.id),

    getPolar: (windSpeed) => routes.Polars.forWind(data.race.id, windSpeed),

    routingSim: (from, to) => routes.Simulations.run(from.lat, from.lon, to.lat, to.lon),

    isUserBoat: (id) => id == data.status.boat.id
  };
}

module.exports = { 
  data: data,
  api: api
};