
var Bacon   = require('baconjs')
var $       = require('jquery');
var bjq     = require('bacon.jquery');
var appData = require('./appData');
var util    = require('./util');

var api  = appData.api();

function streams(queue, map, models) {

  var autoRefreshes = Bacon.interval(60 * 1000).map(true);

  // forecasts display
  Bacon.mergeAll(Bacon.once(true), map.redraws, models.term.changes())
    .doAction(() => map.updateForecastCells())
    .map(() => map.getBounds())
    .map(b => api.getBoundPoints(models.term.get(), b.northWest, b.southEast))
    .ajax()
    .onValue(windFeatures => {
      map.updateForecastPoints(windFeatures);
      models.forecastGenTime.set(windFeatures.properties.genTime);
    });

  // race status updates (periodic & manual)
  Bacon.mergeAll(map.refreshes, autoRefreshes, Bacon.once(true))
    .map(() => api.getRaceStatus())
    .doAction(() => map.startRefresh())
    .ajax()
    .onValue(raceStatus => {
      map.stopRefresh();
      models.status.set(raceStatus.user);
      models.fleet.set(raceStatus.fleet);
    });

  models.zoom.addSource(map.zooms);

  // periodic ranking update
  models.rankings.addSource(Bacon.once(true).merge(autoRefreshes).map(api.getLeaderboard).ajax());

  // simulate speed on direction changes
  models.simDirection.changes().onValue(d => {
    var windAngle = util.windAngle(models.windOrigin.get(), d);
    models.simSpeed.set(util.boatSpeed(windAngle, models.polar.get()));
  });

  // update polar on wind speed change
  models.polar.addSource(models.windSpeed.changes().map(api.getPolar).map($.ajax));

  models.status.changes().onValue(s => map.updateUserBoat(s, models.simulation.get()));

  // real position updates
  models.position.changes().onValue(function(p) {
    if (models.simActive.get()) {
      map.showNextPosition(p, models.simNextPosition.get());
    }
  });

  // real direction update resets simulation
  models.direction.changes().onValue(d => 
    models.simDirection.set(d));

  // fleet update
  models.fleet.changes().onValue(f => 
    map.updateFleet(f));

  // forecast term selection
  models.term.addSource(map.forecastChanges);
  models.term.onValue(t => map.selectTerm(t));

  models.forecastGenTime.changes().onValue(t => 
    map.updateForecastGenTime(t));

  // activate simulation
  models.simDirection.changes().onValue(d =>
    models.simActive.set(true));

  // simulate boat direction on map
  models.simulation.changes().onValue(sim => 
    map.updateUserBoat(models.status.get(), sim));

  // simulate next position on map
  models.simNextPosition.addSource(models.simSpeed.changes().map(s => {
    return util.nextPosition(models.position.get(), models.simDirection.get(), s, 3600 * 6);
  }));

  models.simNextPosition.changes().onValue(p =>
    map.showNextPosition(models.position.get(), p));

  models.simActive.changes().filter(a => !a).onValue(() => map.cancelSimulation());

  var boatFocus = Bacon.mergeAll(queue.ofType('focusBoat').map('.id'), map.boatClicks);
  models.focusedBoatStatus.addSource(boatFocus.flatMap(id => {
    return api.isUserBoat(id) ?
      Bacon.once(models.status.get()) :
      Bacon.once(api.getBoatStatus(id)).ajax();
  }));

  models.focusedBoatStatus.changes().onValue(status => {
    map.focusOnBoat(status);
  });

  models.contextPosition.addSource(map.rightClicks);
  models.contextPosition.changes().onValue(p => {
    map.showContextualMarker(p);
  });

  queue.ofType('submitSim').map(() => 
    api.turnBoat(models.simDirection.get())
  ).ajax().onValue(status => {
    models.simActive.set(false);
    map.updateUserBoat(status, models.simulation.get());
  });

  queue.ofType('cancelSim').onValue(() => {
    models.simDirection.set(models.direction.get());
    models.simActive.set(false);
  });

  queue.ofType('routingSim')
    .doAction(() => {
      models.routing.inProgress.set(true);
      models.routing.bestRoute.set(null);
    })
    .map(msg => api.routingSim(models.position.get(), msg.to)).ajax()
    .onValue((routing) => {
      models.routing.inProgress.set(false)
      models.routing.bestRoute.set(routing.bestRoute);
      map.showBestRoute(routing);
    });

}

module.exports = streams;