
var React          = require('react');
var Bacon          = require('baconjs');
var appData        = require('./appData');
var ControlSidebar = require('./components/controlSidebar');
var InfoSidebar    = require('./components/infoSidebar');
var MapBuilder     = require('./map/builder');
var models         = require('./models').init();
var streams        = require('./streams');

var data = appData.data;
  
var queue = new Bacon.Bus();
queue.ofType = (type) => queue.filter(msg => msg.type == type);
queue.onValue(m => console.log("new message:", m));

var controlSidebar = React.renderComponent(<ControlSidebar models={models} data={data} queue={queue} />, document.getElementById("controlSidebar"));
var infoSidebar = React.renderComponent(<InfoSidebar models={models} data={data} queue={queue} />, document.getElementById("infoSidebar"));

var map = MapBuilder.build(models);

streams(queue, map, models);