
var React      = require('react');
var BaconMixin = require('react-bacon').BaconMixin;
var _          = require('lodash');
var moment     = require('moment');
var util       = require('../util');

var RankingTable = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      rankings: {
        ranks: []
      }
    }
  },

  componentWillMount: function() {
    this.plug(this.props.models.rankings, 'rankings');
  },

  focusBoat: function(id) {
    return function(e) {
      e.preventDefault();
      this.props.queue.push({ type: 'focusBoat', id: id});
    }.bind(this);
  },

  render: function() {

    if (this.state.rankings.time) {

      var time = moment(this.state.rankings.time).format("D MMMM HH:mm");

      var rows = _.map(this.state.rankings.ranks, function(r) {
        var dm = Math.round(util.meterToMile(r.distance));
        return <tr key={r.id}>
          <th>{r.rank}</th>
          <td><a href="" onClick={this.focusBoat(r.id)}>{r.name}</a></td>
          <td>{dm} mn</td>
        </tr>
      }, this);

      return (
        <div className="rankings">
          <p>
            <em>Classement du {time}</em>
          </p>
          <table className="ranking-table">
            <tr>
              <th>#</th>
              <th>Bateau</th>
              <th>Distance</th>
            </tr>
            <tbody>{rows}</tbody>
          </table>      
        </div>
      );

    } else {
      return <div>Chargement en cours...</div>
    }
  }

});

module.exports = RankingTable;
