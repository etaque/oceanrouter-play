
var React      = require('react');
var BaconMixin = require('react-bacon').BaconMixin;
var Geo        = require('geo');
var util       = require('../util');

var BoatDataTable = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      boatPosition: this.props.models.position.get(),
      sim: {
        direction: 0
      },
      windOrigin: 0,
      windSpeed: 0
    };
  },

  getDefaultProps: function() {
    return {
      boatName: "NC"
    };
  },

  componentWillMount: function() {
    this.plug(this.props.models.simulation, 'sim');
    this.plug(this.props.models.position, 'boatPosition');
    this.plug(this.props.models.rank, 'rank');
    this.plug(this.props.models.windOrigin, 'windOrigin');
    this.plug(this.props.models.windSpeed, 'windSpeed');
  },

  incDirection: function() { this.modifyDirection(+1) },
  decDirection: function() { this.modifyDirection(-1) },

  modifyDirection: function(delta) {
    this.props.models.simDirection.modify(d => util.ensure360(d + delta));
  },

  focusOnBoat: function(e) {
    e.preventDefault();
    this.props.models.focusOnUser();
  },

  render: function() {
    var windAngle = util.windAngle(this.state.windOrigin, this.state.sim.direction);
    var speed = util.round2(util.mpsToKnot(this.state.sim.speed));
    var windSpeed = util.round1(util.mpsToKnot(this.state.windSpeed));

    return (
      <table className="boat-data-table">
        <tr>
          <th>Bateau</th>
          <td><strong><a href="" onClick={this.focusOnBoat}>{this.props.boatName}</a></strong></td>
        </tr>
        <tr>
          <th>Classement</th>
          <td>{util.ranked(this.state.rank)}</td>
        </tr>      
        <tr>
          <th>Position</th>
          <td>{util.toDMS(this.state.boatPosition, "/")}</td>
        </tr>
        <tr>
          <th>Vent</th>
          <td>{windSpeed} nds - {Math.round(this.state.windOrigin)}&deg;</td>
        </tr>        
        <tr>
          <th>Cap</th>
          <td>
            <button className="pure-button btn-direction btn-dec" onClick={this.decDirection}>-</button>
            <span className="sim-direction">{Math.round(this.state.sim.direction)}</span>
            <button className="pure-button btn-direction btn-inc" onClick={this.incDirection}>+</button>
          </td>
        </tr>
        <tr>
          <th>Angle au vent</th>
          <td>{Math.round(windAngle)}&deg;</td>
        </tr>
        <tr>
          <th>Vitesse</th>
          <td>{speed} nds</td>
        </tr>
      </table>
    );
  }
});

module.exports = BoatDataTable;