
var React      = require('react');
var BaconMixin = require('react-bacon').BaconMixin;
var $          = require('jquery');
var d3         = require('d3');
var util       = require('../util');

function windAngleArc(boatDirection, windOrigin) {
  var start, end;

  if (boatDirection - windOrigin + 180 > 0) {
    start = windOrigin ;
    end = boatDirection;
  } else {
    start = boatDirection + 360;
    end = windOrigin;
  }

  return d3.svg.arc()
    .innerRadius(95)
    .outerRadius(100)
    .startAngle(util.toRadians(start))
    .endAngle(util.toRadians(end))  
}

var BoatControlWheel = React.createClass({
  mixins: [BaconMixin], 

  getDefaultProps: function() {
    return {
      center: [140, 140]
    };
  },

  getInitialState: function() {
    return {
      direction: 0,
      windOrigin: 0,
      dragging: false,
      paperPos: { x: 0, y: 0 },
      rel: null
    };
  },

  componentDidUpdate: function(props, state) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.onMouseMove)
      document.addEventListener('mouseup', this.onMouseUp)
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.onMouseMove)
      document.removeEventListener('mouseup', this.onMouseUp)
    }
  },

  componentWillMount: function() {
    this.plug(this.props.models.simDirection, 'direction');
    this.plug(this.props.models.windOrigin, 'windOrigin');
  },

  onMouseDown: function(e) {
    // only left mouse button
    if (e.button !== 0) return;
    var offset = $(this.getDOMNode()).offset();
    var paperPos = {
      x: offset.left,
      y: offset.top
    };
    this.setState({
      dragging: true,
      rel: {
        x: e.pageX - paperPos.x,
        y: e.pageY - paperPos.y
      },
      paperPos: paperPos
    });
    e.stopPropagation();
    e.preventDefault();
  },

  onMouseUp: function(e) {
    this.setState({ dragging: false });
    e.stopPropagation();
    e.preventDefault();
  },

  onMouseMove: function(e) {
    if (!this.state.dragging) return;

    var newRel = {
      x: e.pageX - this.state.paperPos.x,
      y: e.pageY - this.state.paperPos.y
    };

    var angleNow = util.toDegrees(util.angleBetweenPoints(this.props.center, [newRel.x, newRel.y]));
    var angleBefore = util.toDegrees(util.angleBetweenPoints(this.props.center, [this.state.rel.x, this.state.rel.y]));
    var angleDelta = angleNow - angleBefore;

    var newDirection = Math.round(util.ensure360(this.state.direction + angleDelta));

    this.setState({
      rel: newRel
    }, function() {
      this.props.models.simDirection.set(newDirection);
    });

    e.stopPropagation();
    e.preventDefault();
  },

  render: function() {
    var arc = windAngleArc(this.state.direction, this.state.windOrigin);
    return (
      <div className="boat-control-wheel">
        <svg width="280px" height="280px" viewBox="0 0 280 280" onMouseDown={this.onMouseDown}>
          <g>
            <path className="wind-marker" transform={"rotate(" + this.state.windOrigin + ",140,140)"} d="M135,16 L145,16 L140,26 L135,16 Z"></path>
            <path className="boat" transform={"rotate(" + this.state.direction + ",140,140)"} d="M111.922346,210 L167.128514,210 C167.128514,210 169.632989,189.174704 166.150625,154.399823 C162.66826,119.624942 139.896558,63 139.896558,63 C139.896558,63 113.997627,121.129719 111.922347,156.469878 C109.847067,191.810038 111.922346,210 111.922346,210 Z"></path>
          </g>
          <g>
            <path className="wind-arc" d={arc()} transform="translate(140,140)"></path>
          </g>
        </svg>
      </div>
    );
  }
});

module.exports = BoatControlWheel;
