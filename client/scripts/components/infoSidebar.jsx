
var React            = require('react');
var RankingTable     = require('./rankingTable');
var FocusedBoatTable = require('./focusedBoatTable');

var InfoSidebar = React.createClass({

  render: function() {
    return (
      <div className="sidebar">
        <RankingTable models={this.props.models} queue={this.props.queue} />
        <FocusedBoatTable models={this.props.models} />
      </div>
    );
  }

});

module.exports = InfoSidebar;