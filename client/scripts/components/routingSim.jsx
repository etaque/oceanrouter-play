/**
 * @jsx React.DOM
 */

var React      = require('react');
var BaconMixin = require('react-bacon').BaconMixin;
var util       = require('../util');

var RoutingSim = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      destination: null,
      inProgress: false
    };
  },

  componentWillMount: function() {
    this.plug(this.props.models.contextPosition.changes(), 'destination');
    this.plug(this.props.models.routing.inProgress.changes(), 'inProgress');
  },

  launchSim: function(e) {
    this.props.queue.push({ type: 'routingSim', to: this.state.destination });
  },

  render: function() {
    if (!this.state.destination) return <div/>;
    return (
      <div className="sim-routing">
        {util.toDMS(this.state.destination)}
        <button className="sim-run" onClick={this.launchSim} disabled={this.state.inProgress}>Find best route</button>
      </div>
    );
  }

});

module.exports = RoutingSim;