var React = require('react');
var BaconMixin = require('react-bacon').BaconMixin;

var BoatSpeedBar = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      speed: 0
    };
  },

  componentWillMount: function() {
    this.plug(this.props.models.simSpeed, 'speed');
  },

  render: function() {
    var percent = this.state.speed * 100 / this.props.maxSpeed;
    return <div className="boat-speed"><div style={{ width: percent + "%" }} /></div>;
  }
});

module.exports = BoatSpeedBar;
