/**
 * @jsx React.DOM
 */

var React      = require('react');
var BaconMixin = require('react-bacon').BaconMixin;
var Geo        = require('geo');
var util       = require('../util');

var FocusedBoatTable = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      boatStatus: null 
    };
  },

  componentWillMount: function() {
    this.plug(this.props.models.focusedBoatStatus, 'boatStatus');
  },

  render: function() {
    if (!this.state.boatStatus) return <div/>;

    var s = this.state.boatStatus;

    var speed = util.round2(util.mpsToKnot(s.position.speed));
    var windOrigin = s.forecastPoint.wind.origin;
    var windAngle = util.windAngle(s.forecastPoint.wind.origin, s.position.direction);
    var windSpeed = util.round1(util.mpsToKnot(s.forecastPoint.wind.speed));

    return (
      <div className="focused-boat">
        <h3>{s.boat.name} <span>({util.ranked(s.boat.rank)})</span></h3>
        <div className="position">{utils.toDMS(position, "/")}</div>
        <div className="wind">
          Vent : {windSpeed} nds - {Math.round(windOrigin)}&deg;
        </div>
        <table className="focused-boat-table">
          <tr>
            <th>Cap</th>
            <td>{Math.round(s.position.direction)}</td>
          </tr>
          <tr>
            <th>Angle au vent</th>
            <td>{Math.round(windAngle)}&deg;</td>
          </tr>
          <tr>
            <th>Vitesse</th>
            <td>{speed} nds</td>
          </tr>
        </table>
      </div>
    );
  }

});

module.exports = FocusedBoatTable;