
var React            = require('react');
var BaconMixin       = require('react-bacon').BaconMixin;
var BoatSpeedBar     = require('./boatSpeedBar');
var BoatDataTable    = require('./boatDataTable');
var BoatControlWheel = require('./boatControlWheel');
var RoutingSim       = require('./routingSim');

var ControlSidebar = React.createClass({
  mixins: [BaconMixin], 

  getInitialState: function() {
    return {
      simActive: false
    }
  },

  componentWillMount: function() {
    this.plug(this.props.models.simActive, 'simActive');
  },

  submitSim: function() {
    this.props.queue.push({ type: 'submitSim' });
  },

  cancelSim: function() {
    this.props.queue.push({ type: 'cancelSim' });
  },

  render: function() {
    return <div className="sidebar">
      <BoatDataTable models={this.props.models} boatName={this.props.data.status.boat.name} />
      <BoatControlWheel models={this.props.models} queue={this.props.queue} />
      <BoatSpeedBar models={this.props.models} maxSpeed={12} />
      <div className="boat-actions">
        <a className="pure-button btn-submit" disabled={!this.state.simActive} onClick={this.submitSim}>Valider</a>
        <a className="pure-button btn-cancel" disabled={!this.state.simActive} onClick={this.cancelSim}>Annuler</a>
      </div>
      <RoutingSim models={this.props.models} queue={this.props.queue} />
    </div>;
  }
});

module.exports = ControlSidebar;
