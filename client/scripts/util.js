
var _      = require('lodash');
var LatLon = require('latlon');
var Geo    = require('geo');

function round(n, precision) {
  if (precision == null) {
    precision = 0;
  }
  var m = Math.pow(10, precision);
  return Math.round(n * m) / m;
}

var util = {

  ensure360: (d) => (d + 360) % 360,

  windAngle: function(windOrigin, boatDirection) {
    var delta, delta180;
    delta = windOrigin - boatDirection;
    delta180 = (function() {
      switch (false) {
        case !(delta > 180):
          return delta - 360;
        case !(delta <= -180):
          return delta + 360;
        default:
          return delta;
      }
    })();
    return Math.abs(delta180);
  },

  round: round,
  round1: (n) => round(n, 1),
  round2: (n) => round(n, 2),

  knotToMps: (knot) => knot * 1.852 * 1000 / 3600,
  mpsToKnot: (mps) => mps / 1.852 / 1000 * 3600,
  meterToMile: (m) => m / 1.852 / 1000,

  toDegrees: (rad) => rad * (180 / Math.PI),
  toRadians: (deg) => deg * Math.PI / 180,

  boatSpeed: (windAngle, polar) => {
    var p = _.findLast(polar, (p) => p.windAngle <= windAngle);
    if (p) {
      return p.boatSpeed;
    } else {
      return 0;
    }
  },

  // boatSpeedPct: (polar, s) => {
  //   var max = _(polar).map("boatSpeed").max().value();
  //   var pct = s * 100 / max;
  //   return core.round1(pct);
  // },

  angleBetweenPoints: function(p1, p2) {
    if (p1[0] === p2[0] && p1[1] === p2[1]) {
      return Math.PI / 2;
    } else {
      return Math.atan2(p2[1] - p1[1], p2[0] - p1[0]);
    }
  },
  distanceBetweenPoints: function(p1, p2) {
    return Math.sqrt(Math.pow(p2[1] - p1[1], 2) + Math.pow(p2[0] - p1[0], 2));
  },
  nextPosition: function(position, direction, speed, duration) {
    var distanceInKm, from, to;
    distanceInKm = speed * duration / 1000;
    from = new LatLon(position.lat, position.lon);
    to = from.destinationPoint(direction, distanceInKm);
    return {
      lat: to.lat(),
      lon: to.lon()
    };
  },
  rotateWind: function(direction) {
    return (direction + 270) % 360;
  },
  rotateStyle: function(angle) {
    return ("-webkit-transform: rotate(" + angle + "deg);-moz-transform: rotate(" + angle + "deg);") + ("-ms-transform: rotate(" + angle + "deg);-o-transform: rotate(" + angle + "deg);");
  },
  opacityForWind: function(wind) {
    return 0.9;
  },

  eventTargetData: (event) => $(event.target).data(),

  toDMS: (position, sep) => {
    var lat = Geo.toDMS(position.lat) + (position.lat > 0 ? "N" : "S");
    var lon = Geo.toDMS(position.lon) + (position.lon > 0 ? "E" : "W");
    return lat + " " + (sep || "/") + " " + lon;
  },

  ranked: (r) => {
    return r === 1 ? 
      "1er" : 
      r === 2 ? 
        "2nd" : 
        (r === null || r === undefined) ? "NC" : "" + r + "e";    
  }
};

module.exports = util;
