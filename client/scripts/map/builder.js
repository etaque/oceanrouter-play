
var L        = require('leaflet');
var _        = require('lodash');
var $        = require('jquery');
var Bacon    = require('baconjs');
var bjq      = require('bacon.jquery');
var moment   = require('moment');
var util     = require('../util');
var appData  = require('../appData');
var elements = require('./elements');
var controls = require('./controls');
var tpl      = require('./templates');

require('leaflet-providers');

var data = appData.data,
  api    = appData.api();

var Builder = {

  watercolorTileLayer: L.tileLayer.provider('Stamen.Watercolor'),
  osmTileLayer: L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
    attribution: "&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors"
  }),
  mapboxTileLayer: L.tileLayer("http://{s}.tiles.mapbox.com/v3/etaque.gl1dd2gb/{z}/{x}/{y}.png"),

  startMarker: L.marker(data.race.startPosition).bindPopup(tpl.startDescription()),
  finishMarker: L.marker(data.race.finishPosition).bindPopup(tpl.finishDescription()),
  finishCircle: L.circle(data.race.finishPosition, data.config.finishRadius),

  forecastMarkerGroup: new L.LayerGroup(),
  forecastCells: new elements.ForecastCells(),

  fleetMarkerGroup: new L.LayerGroup(),
  focusMarkerGroup: new L.LayerGroup(),
  userMarkerGroup: new L.LayerGroup(),
  userBoatMarker: null,

  fleetControl: new controls.FleetControl(),
  forecastControl: new controls.ForecastControl(),
  nextPositionGroup: new L.LayerGroup(),

  routingSimGroup: new L.LayerGroup(),
  contextualMarker: null,

  getBounds: function() {
    return {
      northWest: this.map.getBounds().getNorthWest(),
      southEast: this.map.getBounds().getSouthEast()
    };
  },

  updateForecastPoints: function(windFeatures) {
    this.forecastMarkerGroup.clearLayers();

    var zoom = this.map.getZoom();

    if (zoom > 5) {
      L.geoJson(windFeatures, {
        pointToLayer: elements.windFeatureToMarker(zoom)
      }).addTo(this.forecastMarkerGroup);

      this.forecastMarkerGroup.addTo(this.map);
    }
  },

  updateForecastCells: function() {
    this.map.removeLayer(this.forecastCells);
    if (this.map.getZoom() > 7) {
      this.map.addLayer(this.forecastCells)
    }
  },

  updateForecastGenTime: function(t) {
    var ft = moment.utc(t).format("D MMMM - HH[h] [UTC]");
    return $(".forecast-term.current").attr("title", ft);
  },

  updateFleet: function(fleetStatus) {
    this.fleetMarkerGroup.clearLayers();
    var isUser = false; // TODO
    _.forEach(fleetStatus, function(status) {
      var m = elements.boatMarker(status, status.turn.direction, isUser)
      m.on('click', e => this.boatClicks.push(status.boat.id));
      m.addTo(this.fleetMarkerGroup);
    }, this);
  },

  updateUserBoat: function(status, sim) {
    this.userMarkerGroup.clearLayers();

    L.circleMarker(status.position, {
      radius: 20,
      color: "#F5A623",
      weight: 1,
      fillOpacity: 0.2
    }).addTo(this.userMarkerGroup);

    this.userBoatMarker = elements.boatMarker(status, sim.direction, true);
    this.userBoatMarker.addTo(this.userMarkerGroup);

    elements.boatRoute(status, true).addTo(this.userMarkerGroup);

    this.userMarkerGroup.addTo(this.map);
  },

  showNextPosition: function(actual, next) {
    this.nextPositionGroup.clearLayers();

    var line = L.polyline([actual, next], {
      color: "#fff",
      weight: 2
    });
    line.addTo(this.nextPositionGroup);

    var point = L.circleMarker(next, {
      radius: 5,
      color: "#fff",
      fillColor: "#fff",
      weight: 1
    });
    point.addTo(this.nextPositionGroup);

    this.nextPositionGroup.addTo(this.map);
  },

  cancelSimulation: function() {
    this.nextPositionGroup.clearLayers();
  },

  findFleetMarker: function(boatId) {
    return _.find(this.fleetMarkerGroup.getLayers(), m => m.boatId === boatId);
  },

  findBoatMarker: function(boatId) {
    if (boatId === data.status.boat.id) {
      return this.userBoatMarker;
    } else {
      return this.findFleetMarker(boatId);
    }
  },

  focusOnBoat: function(status) {
    this.focusMarkerGroup.clearLayers();

    var m = this.findBoatMarker(status.boat.id);

    if (m) {
      elements.boatRoute(status).addTo(this.focusMarkerGroup);
      this.setFocusedBoatMarker(status.boat.id);
      this.map.panTo(m.getLatLng());
    }
  },

  setFocusedBoatMarker: function(boatId) {
    $(".boat-marker").removeClass("boat-marker-focused");
    $(".boat-marker-" + boatId).addClass("boat-marker-focused");
  },

  showContextualMarker: function(point) {
    if (this.contextualMarker) {
      this.contextualMarker.setLatLng(point);
    } else {
      this.contextualMarker = L.marker(point);
      this.contextualMarker.addTo(this.map);
    }
  },

  showBestRoute: function(routing) {
    this.routingSimGroup.clearLayers();

    var positions = _.map(routing.path, 'position').concat(routing.journey.dest);
    var line = L.polyline(positions, {
      className: 'routing-sim-path'
    });
    line.addTo(this.routingSimGroup);

    _.forEach(routing.path, function(step) {
      var point = L.circleMarker(step.position, {
        className: 'routing-sim-points',
        title: "test",
        radius: 2
      });
      point.bindPopup("Vent : " + step.windSpeed); // TODO
      point.addTo(this.routingSimGroup);
    }, this);

    this.routingSimGroup.addTo(this.map);
  },

  selectTerm: function(term) {
    $('.forecast-control a').removeClass("current");
    $('.forecast-control a[data-term='+term+']').addClass("current");
  },

  startRefresh: function() {
    $(".btn-refresh").addClass("btn-loading");
  },

  stopRefresh: function() {
    $(".btn-refresh").removeClass("btn-loading");
  },

  build: function(models) {

    this.models = models;

    L.Icon.Default.imagePath = '/assets/leaflet/images';

    this.map = L.map("map", {
      zoomControl: false
    });

    this.map
      .addLayer(this.mapboxTileLayer)
      .addControl(this.fleetControl)
      .addControl(this.forecastControl)
      .addControl(L.control.zoom({ position: 'topright' }))
      .addControl(
        L.control.layers({
          Terrain: this.mapboxTileLayer,
          Aquarelle: this.watercolorTileLayer
        }, {}, { position: "bottomright" })
      )
      .addLayer(elements.orthodromicLine(data.race.startPosition, data.race.finishPosition))
      .addLayer(this.fleetMarkerGroup)
      .addLayer(this.focusMarkerGroup)
      .setView(this.models.position.get(), this.models.zoom.get());

    this.startMarker.addTo(this.map);
    this.finishMarker.addTo(this.map);
    this.finishCircle.addTo(this.map);

    var $map = $('#map');

    this.redraws = Bacon.mergeAll(_.map(["resize", "moveend", "zoomend"], n => {
      return Bacon.fromEventTarget(this.map, n);
    }));

    this.zooms = Bacon.fromEventTarget(this.map, "zoomend").map(e => e.target.getZoom());

    this.forecastChanges = $map.asEventStream("click", ".forecast-term").map(event =>
      parseInt($(event.target).data().term));
    
    this.refreshes = $map.asEventStream("click", ".btn-refresh").doAction(".preventDefault").map(true);

    this.boatClicks = new Bacon.Bus();

    this.rightClicks = Bacon.fromEventTarget(this.map, "contextmenu").map('.latlng').map(ll => { 
      return { lat: ll.lat, lon: ll.lng }
    });

    return this;
  }
}

module.exports = Builder;
