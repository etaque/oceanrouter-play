
var _    = require('lodash');
var L    = require('leaflet');
var util = require('../util');
var tpl  = require('./templates');


var FleetControl = L.Control.extend({
  options: {
    position: "topright"
  },
  onAdd: function() {
    var btnFleet, btnLocate, btnRefresh, container;

    container = L.DomUtil.create("div", "leaflet-control horizontal-control fleet-control");

    btnRefresh = L.DomUtil.create("a", "btn-icon btn-refresh", container);
    btnRefresh.href = "#";
    btnRefresh.innerHTML = "<span>&nbsp;</span>";

    return container;
  }
});

var ForecastControl = L.Control.extend({
  options: {
    position: "topleft"
  },
  onAdd: function() {
    var container = L.DomUtil.create("div", "leaflet-control horizontal-control forecast-control");
    container.innerHTML = tpl.forecasts();
    return container;
  }
});


module.exports = {
  FleetControl: FleetControl,
  ForecastControl: ForecastControl
};