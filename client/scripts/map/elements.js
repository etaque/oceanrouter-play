
var L    = require('leaflet');
var _    = require('lodash');
var arc  = require('arc');
var util = require('../util');
var tpl  = require('./templates');

var elements = {

  windFeatureToMarker: _.curry(function(zoom, feature, latlng) {

    var fontSize, label, opacity, windClass, windDirection, windIcon, windOrigin, windSpeed;

    windDirection = Math.round(feature.properties.wind.direction);
    windOrigin = Math.round(feature.properties.wind.origin);
    windSpeed = util.round2(feature.properties.wind.speed);

    label = "Origine : " + windOrigin.toString() + "&deg;, force : " + Math.round(util.mpsToKnot(windSpeed)).toString() + " noeuds";
    opacity = util.opacityForWind(windSpeed);
    fontSize = Math.round(windSpeed * zoom / 4);

    windClass = "_f" + Math.min(Math.ceil(Math.round(windSpeed * 8 / 20)), 8);

    windIcon = L.divIcon({
      className: "wind-marker",
      html: "<div title=\"" + label + "\" style=\"" + util.rotateStyle(util.rotateWind(windDirection)) + "opacity:" + opacity.toString() + ";font-size: " + fontSize + "px;\" class=\"" + windClass + "\">&#8594;</div>"
    });

    return L.marker(latlng, {
      icon: windIcon
    });
  }),


  ForecastCells: L.GeoJSON.extend({
    options: {
      interval: 0.5,
      style: {
        color: "#fff",
        weight: 1,
        opacity: 0.2
      }
    },

    initialize: function(options) {
      L.Util.setOptions(this, options);
      this._layers = {};
      this.addData(this.getCells());
    },

    getCells: function() {
      var interval, meridians, parallels;
      interval = this.options.interval;

      meridians = _.map(_.range(-180, 180, interval), (function(_this) {
        return function(i) {
          return _this.getFeature(_this.getMeridian(i + interval / 2));
        };
      })(this));

      parallels = _.map(_.range(-90, 90, interval), (function(_this) {
        return function(i) {
          return _this.getFeature(_this.getParallel(i + interval / 2));
        };
      })(this));

      return {
        "type": "FeatureCollection",
        "features": meridians.concat(parallels)
      };
    },

    getMeridian: function(lon) {
      return _.map(_.range(-90, 90), function(lat) {
        return [lon, lat];
      });
    },

    getParallel: function(lat) {
      return _.map(_.range(-180, 180), function(lon) {
        return [lon, lat];
      });
    },

    getFeature: function(coords, prop) {
      if (prop == null) {
        prop = {};
      }
      return {
        type: "Feature",
        geometry: {
          type: "LineString",
          coordinates: coords
        },
        properties: prop
      };
    }
  }),

  boatMarker: function(status, direction, isUser) {

    var boatIcon = L.divIcon({
      className: "boat-marker boat-marker-" + status.boat.id + " " + (isUser ? "user-boat-marker" : ""),
      iconSize: new L.Point(20, 20),
      html: '<div id="iconBoat' + status.boat.id + '" style="' + util.rotateStyle(direction) + '"></div>'
    });

    var marker = L.marker([status.position.lat, status.position.lon], {
      icon: boatIcon
    });

    marker.boatStatus = status;
    marker.boatId = status.boat.id;

    return marker;
  },

  boatRoute: function(status, isUser) {

    var points = _.map(status.route, p => new L.LatLng(p.lat, p.lon));

    var line = L.polyline(points, {
      lineJoin: "round",
      className: isUser ? "boat-route user-boat-route" : "boat-route"
    });

    line.boatId = status.boat.id;

    return line;
  },

  orthodromicLine: function(startPosition, finishPosition) {

    var start = { x: startPosition.lon, y: startPosition.lat };
    var finish = { x: finishPosition.lon, y: finishPosition.lat };

    var gc = new arc.GreatCircle(start, finish);
    var geoArc = gc.Arc(50);

    var points = _.map(geoArc.geometries[0].coords, c => new L.LatLng(c[1], c[0]));

    return L.polyline(points, {
      color: "#a30000",
      weight: 1,
      dashArray: "5, 5, 1, 5",
      opacity: 0.8,
      label: "Route orthodromique"
    });
  }
};

module.exports = elements;
