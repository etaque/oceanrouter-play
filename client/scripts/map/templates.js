
var $          = require('jquery');
var Handlebars = require('handlebars');

function cpl(id) {
  return Handlebars.compile($(id).html());
};

var templates = {
  startDescription: cpl("#tpl-start-description"),
  finishDescription: cpl("#tpl-finish-description"),
  forecasts: cpl("#tpl-forecasts")
};

module.exports = templates;